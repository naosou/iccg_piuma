#include "precision.h"

      module PCG_ELL
      use iso_c_binding

      integer, parameter :: N2= 256
      integer :: NUmax, NLmax, NCOLORtot, NCOLORk, NU, NL
      integer :: NPL, NPU
      integer :: METHOD, ORDER_METHOD, NFLAG

      real(kind=8) :: EPSICCG

      vec_cg_f, dimension(:), allocatable,target:: D, PHI, BFORCE
      mat_cg_f, dimension(:,:), allocatable, target :: AL, AU
      mat_cg_f, dimension(:,:), allocatable :: AL0, AU0

      integer, dimension(:), allocatable :: INL, INU, COLORindex
      integer, dimension(:), allocatable :: SMPindex, SMPindexG
      integer, dimension(:), allocatable :: OLDtoNEW, NEWtoOLD

      integer, dimension(:,:), allocatable :: IAL, IAU

      integer, dimension(:), allocatable, target :: SMPindex_new
      integer, dimension(:  ), allocatable :: INLnew, INUnew
      integer, dimension(:,:), allocatable :: IALnew, IAUnew

      integer, dimension(:), allocatable :: OLDtoNEWnew, NEWtoOLDnew

      integer, dimension(:),   allocatable, target :: indexL, indexU
      integer, dimension(:,:), allocatable, target ::  itemL,  itemU
      integer, dimension(:),   allocatable :: indexL0, indexU0
      integer, dimension(:,:), allocatable ::  itemL0,  itemU0

      end module PCG_ELL
