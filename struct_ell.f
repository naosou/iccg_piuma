#include "precision.h"

      module STRUCT_ELL

      use OMP_LIB
      use iso_c_binding

!C
!C-- METRICs & FLUX
      integer (kind=kint) :: ICELTOT, ICELTOTp, N
      integer (kind=kint) :: NX, NY, NZ, NXP1, NYP1, NZP1, IBNODTOT
      integer (kind=kint) :: NXc, NYc, NZc

      mat_cg_f :: DX, DY, DZ, XAREA, YAREA, ZAREA, RDX, RDY, RDZ,         &
     &                                RDX2, RDY2, RDZ2, R2DX, R2DY, R2DZ

      mat_cg_f,
     &     dimension(:), allocatable :: VOLCEL, VOLNOD, RVC, RVN

      integer (kind=kint), dimension(:,:), allocatable :: XYZ, NEIBcell

!C
!C-- BOUNDARYs
      integer (kind=kint) :: ZmaxCELtot
      integer (kind=kint), dimension(:), allocatable :: BC_INDEX, BC_NOD
      integer (kind=kint), dimension(:), allocatable :: ZmaxCEL

!C
!C-- WORK
      integer (kind=kint), dimension(:,:), allocatable :: IWKX
      mat_cg_f, dimension(:,:), allocatable :: FCV

!C
!C-- MPI
      integer (kind=kint) :: my_rank, SOLVER_COMM, PETOT, PEsmpTOT

      end module STRUCT_ELL
