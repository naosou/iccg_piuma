#include<stdlib.h>
#include<stdio.h>

#include <omp.h>

#include "math.h"

#include "precision.h"

#if vec_precon_bit == vec_cg_bit
#define TGT_PRECON W
#else
#define TGT_PRECON WZ
#define flag_prepost
#endif

void solver_ICCG_CRS(int N, int N2, int NPL, int NPU, int *indexL, int *itemL, int *indexU, int *itemU,
		     mat_cg *D, vec_cg *B, vec_cg *X, mat_cg *AL, mat_cg *AU,
		     int NCOLORtot, int PEsmpTOT, int *SMPindex, double EPS, int ITR, double IER)
{
  mat_precon *Ws_DD;
  mat_precon *AL_pre, *AU_pre;

  int R = 1, Z = 0, Q = 0, P = 2, DD = 3;

  double W_RHO[PEsmpTOT], W_C1[PEsmpTOT], W_DNRM2[PEsmpTOT];

  vec_precon VAL_p, SW_p;
  vec_cg VAL;

  int nth, ls, rmin;

  int i, ii, k, L, is, js, ib, ib0, ic, ip, ip0, iq0, ip1, iq1, iq2;
  vec_cg C1, ALPHA, BETA, RHO, RHO1, ERR, ERR2, BNRM2, DNRM2;

  double Stime, Etime, time_precon = 0.0;

  char nvp[10] = name_vec_precon, nmp[10]  = name_mat_precon;

  FILE *fp;
  
//;
// +------+;
// | INIT |;
// +------+;
//;

  printf("solver_ICCG_CRS, %s-%s\n", nmp, nvp);

  vec_cg (*W)[N+N2];
  if((W = (double(*)[N+N2])malloc((N+N2) * 4 * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  if((Ws_DD = (double *)malloc((N+N2) * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  vec_cg (*WZ)[N+N2];
#ifdef flag_prepost
  if((WZ = (double(*)[N+N2])malloc((N+N2) * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
#endif
  if((AL_pre = (double *)malloc((NPL) * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  if((AU_pre = (double *)malloc((NPL) * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  
#pragma omp parallel private(ip,i)
  {
    ip = omp_get_thread_num();
    for(i=SMPindex[ip*NCOLORtot]; i < SMPindex[(ip+1)*NCOLORtot]; i++){
      X[i]    = 0.0;
      W[1][i] = 0.0;
      W[2][i] = 0.0;
      W[3][i] = 0.0;
#ifdef flag_prepost
      WZ[Z][i] = 0.0;
#endif
    }
  }
  
#pragma omp parallel default(none) \
  shared(SMPindex, AL, D, Ws_DD, itemL, indexL)	\
  private(ip,ip1,ic,i,VAL,k) \
  firstprivate(NCOLORtot)
  {
    ip = omp_get_thread_num();
    for(ic=0; ic<NCOLORtot; ic++){
      ip1 = ip*NCOLORtot + ic;
      for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++){
	VAL = D[i];
	for(k = indexL[i]; k < indexL[i+1]; k++){
	  VAL= VAL - (AL[k]*AL[k]) * Ws_DD[itemL[k]];
	  // if(i == 45882) printf("Check Ws_DD=%lf, AL=%lf, VAL=%lf\n",Ws_DD[itemL[k]], AL[k], VAL);
	}
	// if(i == 45882) printf("Check VAL=%lf, D=%lf\n", VAL, D[i]);
	Ws_DD[i]= 1.0/VAL;
      }
#pragma omp barrier
    }
  }
  
#pragma omp parallel default(none) \
  shared(AL, AU, AL_pre, AU_pre, SMPindex, indexU, indexL)	\
  private(ic, i, k, ip, ip1)					\
  firstprivate(NCOLORtot)
  {
    ip = omp_get_thread_num();
    for(ic = 0; ic < NCOLORtot; ic++){
      ip1 = ip*NCOLORtot + ic;
      for(i = SMPindex[ip1]; i< SMPindex[ip1+1]; i++)
	for(k = indexL[i]; k< indexL[i+1]; k++)
	  AL_pre[k] = AL[k];
    }
    for(ic = NCOLORtot-1; ic >=0; ic--){
      ip1 = ip*NCOLORtot + ic;
      for(i = SMPindex[ip1]; i< SMPindex[ip1+1]; i++)
	for(k = indexU[i]; k< indexU[i+1]; k++)
	  AU_pre[k] = AU[k];
    }
  }
  
//C;
//C +-----------------------+;
//C | {r0}= {b} - [A]{xini} |;
//C +-----------------------+;
//C===;

  BNRM2= 0.0;
#pragma omp parallel for private(i) reduction(+:BNRM2)
  for(i = 0; i < N; i++){
    W[R][i]= B[i];
    BNRM2 = BNRM2 + B[i] * B[i];
  }
//C===
  Stime= omp_get_wtime();
//C***************************************************************  ITERATION;
  ITR = N;

#pragma omp parallel default(none)					\
  shared(B, X, D, AL, AU, AL_pre, AU_pre, W, Ws_DD, itemL, itemU,	\
	 indexL, indexU, WZ, W_RHO, W_C1, W_DNRM2, SMPindex,		\
	 time_precon, RHO1, IER, ITR, ERR2, fp)				\
  private(ic,ip,ip0,ip1,iq0,iq1,iq2,ib,ib0,is,i,k,rmin,L,		\
	  nth,ls,VAL,VAL_p,SW_p,RHO,DNRM2,C1,ERR,ALPHA,BETA)		\
  firstprivate(N, NCOLORtot, BNRM2, EPS, R, Z, Q, P, DD)
  {
    ip = omp_get_thread_num();
    nth= omp_get_num_threads();
    ls = (N+nth-1)/nth;
    rmin = (ip+1)*ls<N ? (ip+1)*ls : N;

    for(L=0; L<ITR; L++)
      {
//C
//C +----------------+
//C | {z}= [Minv]{r} |
//C +----------------+
//C===
	for(i = ip*ls; i < rmin; i++)
	  TGT_PRECON[Z][i] = W[R][i];
#pragma omp barrier
	if(ip == 0) time_precon = time_precon - omp_get_wtime();
	
	for(ic = 0; ic < NCOLORtot; ic++){
	  ip1 = ip*NCOLORtot + ic;
	  for(i = SMPindex[ip1]; i < SMPindex[ip1+1]; i++){
	    VAL_p = TGT_PRECON[Z][i];
	    // if(i == 9829) printf("Check range %d, %d\n", indexL[i], indexL[i+1]);
	    for(k = indexL[i]; k < indexL[i+1]; k++){
	      VAL_p = VAL_p -  AL_pre[k] * TGT_PRECON[Z][itemL[k]];
	      // if(i == 81931) printf("Check mat %d, %d, %d, %lf, %d, %lf, %lf\n", ic, i, k, AL_pre[k], itemL[k], TGT_PRECON[Z][itemL[k]], VAL_p);
	    }
	    TGT_PRECON[Z][i] = VAL_p * Ws_DD[i];
	    // if(i == 81931) printf("Check mat %d, %d, %lf, %lf\n", ic, i, Ws_DD[i], VAL_p);
	  }
#pragma omp barrier
	}

/* #pragma omp master */
/* 	{ */
/* 	  if ((fp = fopen("forward_C.txt", "w")) == NULL) { */
/* 	    printf("file open error!!\n"); */
/* 	    exit(EXIT_FAILURE); */
/* 	  } */
/* 	  for(i=0;i<N;i++) */
/* 	    fprintf(fp, "%7d%12.6lf\n", i+1, TGT_PRECON[Z][i]); */
/* 	  fclose(fp); */
/* 	} */
/* #pragma omp barrier */
	
	
	for(ic = NCOLORtot-1; ic >= 0; ic--){
	  ip1 = ip*NCOLORtot + ic;
	  for(i = SMPindex[ip1]; i < SMPindex[ip1+1]; i++){
	    SW_p = 0.0;
	    for(k = indexU[i]; k < indexU[i+1]; k++)
	      SW_p = SW_p +  AU_pre[k] * TGT_PRECON[Z][itemU[k]];
            TGT_PRECON[Z][i]= TGT_PRECON[Z][i] - Ws_DD[i] * SW_p;
	  }
#pragma omp barrier
	}

/* #pragma omp master */
/* 	{ */
/* 	  if ((fp = fopen("backward_C.txt", "w")) == NULL) { */
/* 	    printf("file open error!!\n"); */
/* 	    exit(EXIT_FAILURE); */
/* 	  } */
/* 	  for(i=0;i<N;i++) */
/* 	    fprintf(fp, "%7d%12.6lf\n", i+1, TGT_PRECON[Z][i]); */
/* 	  fclose(fp); */
/* 	} */
/* #pragma omp barrier */
	
	if(ip == 0) time_precon = time_precon + omp_get_wtime();

//C===;

//C;
//C +-------------+;
//C | RHO= {r}{z} |;
//C +-------------+;
//C===;
	W_RHO[ip]= 0.0;
#pragma omp simd            
	for(i = ip*ls; i < rmin; i++)
	  W_RHO[ip]= W_RHO[ip] + W[R][i]*TGT_PRECON[Z][i];
	RHO = 0.0;
#pragma omp barrier
#pragma omp simd
	for(i=0; i < nth; i++)
	  RHO= RHO + W_RHO[i];
	// if(ip == 0) printf("Check rho = %lf\n",RHO);
//C===;

//C;
//C +-----------------------------+;
//C | {p} = {z} if      ITER=1    |;
//C | BETA= RHO / RHO1  otherwise |;
//C +-----------------------------+;
//C===;
	if(L == 0){
#pragma omp simd
	  for(i = ip*ls; i < rmin; i++)
	    W[P][i] = TGT_PRECON[Z][i];
	}else{
	  BETA = RHO / RHO1;
#pragma omp simd
	  for(i = ip*ls; i < rmin; i++)
	    W[P][i] = W[Z][i] + BETA * TGT_PRECON[P][i];
      }
#pragma omp barrier
	// if(ip == 0) printf("Check beta = %lf\n",BETA);

//C;
//C +-------------+;
//C | {q}= [A]{p} |;
//C +-------------+;
//C===        ;

	for(i = ip*ls; i < rmin; i++){
	  VAL = D[i]*W[P][i];
	  for(k = indexL[i]; k < indexL[i+1]; k++)
	    VAL = VAL + AL[k] * W[P][itemL[k]];
	  for(k = indexU[i]; k < indexU[i+1]; k++)
	    VAL= VAL + AU[k]*W[P][itemU[k]];
	  W[Q][i] = VAL;
	}
#pragma omp barrier
//C===;

//C;
//C +---------------------+;
//C | ALPHA= RHO / {p}{q} |;
//C +---------------------+;
//C===;
	W_C1[ip]= 0.0;
#pragma omp simd            
	for(i = ip*ls; i < rmin; i++)
	  W_C1[ip]= W_C1[ip] + W[P][i] * W[Q][i];
	C1 = 0.0;
#pragma omp barrier
#pragma omp simd
	for(i=0; i < nth; i++)
	  C1= C1 + W_C1[i];
	ALPHA= RHO / C1;
	// if(ip == 0) printf("Check alpha = %lf\n",ALPHA);
//C===;


//C;
//C +----------------------+;
//C | {x}= {x} + ALPHA*{p} |;
//C | {r}= {r} - ALPHA*{q} |;
//C +----------------------+;
//C===;
	W_DNRM2[ip] = 0.0;
#pragma omp simd
	for(i = ip*ls; i < rmin; i++){
	  X[i] = X[i] + ALPHA * W[P][i];
	  W[R][i]= W[R][i] - ALPHA * W[Q][i];
	  W_DNRM2[ip]= W_DNRM2[ip] + W[R][i] * W[R][i];
	}
	DNRM2 = 0.0;
#pragma omp barrier
#pragma omp simd
	for(i=0; i < nth; i++)
	  DNRM2= DNRM2 + W_DNRM2[i];
	// if(ip == 0) printf("Check DNRM2=%lf, BNRM2=%lf\n",DNRM2,BNRM2);
//C===;
	
	ERR = sqrt(DNRM2/BNRM2);
	if (L%100 == 0){
#pragma omp single
	  printf("%i, %e\n", L, ERR);
	}
      
	// if(ip == 0) printf("Check ERR=%e, EPS=%e\n", ERR, EPS);
	  if(ERR < EPS){
	  IER = 0;
	  break;
	}else{
	  RHO1 = RHO;
	}
	
      }

    IER = 1;
    if(ip == 0){
      ITR = L;
      ERR2 = ERR;
    }
  }

  Etime= omp_get_wtime();
  
  printf("Converged, %i, %e\n", ITR, ERR2);
  printf("%lf sec. (solver)\n", Etime-Stime);
  printf("%lf sec. (preconditioner)\n", time_precon);
}
  
