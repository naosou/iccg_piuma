#include "precision.h"
      
!C
!C***
!C*** POI_GEN
!C***
!C
!C    generate COEF. MATRIX for POISSON equations
!C    

      subroutine POI_GEN_SCS

      use STRUCT_SCS
      use PCG_SCS

!C      implicit REAL*8 (A-H,O-Z)
      implicit none

      integer i, j, k, ii, jj, kk
      integer ic, ip, ip1, ip2, up2, icel, icelN, ic0, ic01, ic02, ic0a,
     &                  ik0, icNS, icou, ib, ib0, is, ip0, iq1, iq2, iq0 
      integer j0, j1, js, padding_NSIMD
      integer kL, kU
      integer nn, nn1, nnbk, nnn, nr, num
      integer icN1, icN2, icN3, icN4, icN5, icN6
      integer icN10,icN20,icN30,icN40, icN50,icN60
      integer NBK, NBKmax, NBKmaxALL, NBKtot

      real(kind=4) coef, c0, c1
      temp_mat_precon_f EM1, EM2, EM3
      real(kind=8) E1t, S1t

!C
!C-- INIT.
      nn = ICELTOT

      NL= 6
      NU= 6

      allocate (INL(nn), INU(nn), IAL(NL,nn), IAU(NU,nn))

      INL= 0
      INU= 0
      IAL= 0
      IAU= 0

!C
!C-- INTERIOR & NEUMANN boundary's
      do icel= 1, ICELTOT
        icN1= NEIBcell(icel,1)
        icN2= NEIBcell(icel,2)
        icN3= NEIBcell(icel,3)
        icN4= NEIBcell(icel,4)
        icN5= NEIBcell(icel,5)
        icN6= NEIBcell(icel,6)

        if (icN5.ne.0) then
          icou= INL(icel) + 1
          IAL(icou,icel)= icN5
          INL(     icel)= icou
        endif

        if (icN3.ne.0) then
          icou= INL(icel) + 1
          IAL(icou,icel)= icN3
          INL(     icel)= icou
        endif

        if (icN1.ne.0) then
          icou= INL(icel) + 1
          IAL(icou,icel)= icN1
          INL(     icel)= icou
        endif

        if (icN2.ne.0) then
          icou= INU(icel) + 1
          IAU(icou,icel)= icN2
          INU(     icel)= icou
        endif

        if (icN4.ne.0) then
          icou= INU(icel) + 1
          IAU(icou,icel)= icN4
          INU(     icel)= icou
        endif

        if (icN6.ne.0) then
          icou= INU(icel) + 1
          IAU(icou,icel)= icN6
          INU(     icel)= icou
        endif

      enddo
!C===

!C
!C +---------------+
!C | MULTICOLORING |
!C +---------------+
!C===
      allocate (OLDtoNEW(ICELTOT), NEWtoOLD(ICELTOT))
      allocate (OLDtoNEWnew(ICELTOT), NEWtoOLDnew(ICELTOT))
      allocate (COLORindex(0:ICELTOT))
      OLDtoNEWnew= 0
      NEWtoOLDnew= 0

 111    continue
        write (*,'(//a,i8,a)') 'You have', ICELTOT, ' elements.'
        write (*,'(  a     )') 'How many colors do you need ?'
        write (*,'(  a     )') '  #COLOR must be more than 2 and'
        write (*,'(  a,i8  )') '  #COLOR must not be more than', ICELTOT
        write (*,'(  a     )') '   CM if #COLOR .eq. 0'
        write (*,'(  a     )') '  RCM if #COLOR .eq.-1'
        write (*,'(  a     )') 'CMRCM if #COLOR .le.-2'
        write (*,'(  a     )') '=>'

        if (NCOLORtot.gt.0) goto 111

      if (NCOLORtot.eq.0) then
        call  CM (ICELTOT, NL, NU, INL, IAL, INU, IAU,                  &
     &            NCOLORtot, COLORindex, NEWtoOLD, OLDtoNEW)
	write (*,'(//a)') '###  CM' 
      endif

      if (NCOLORtot.eq.-1) then
        call RCM (ICELTOT, NL, NU, INL, IAL, INU, IAU,                  &
     &            NCOLORtot, COLORindex, NEWtoOLD, OLDtoNEW)
	write (*,'(//a)') '### RCM' 
      endif

      if (NCOLORtot.lt.-1) then
        call CMRCM (ICELTOT, NL, NU, INL, IAL, INU, IAU,                &
     &            NCOLORtot, COLORindex, NEWtoOLD, OLDtoNEW)
	write (*,'(//a)') '### CM-RCM' 
      endif

      write (*,'(//a,i8,// )') '### FINAL COLOR NUMBER', NCOLORtot

      allocate (SMPindex(0:PEsmpTOT*NCOLORtot))
      SMPindex= 0
      do ic= 1, NCOLORtot
        nn1= COLORindex(ic) - COLORindex(ic-1)
        ! num= nn1 / PEsmpTOT
        ! nr = nn1 - PEsmpTOT*num
        nr = mod(nn1, PEsmpTOT)
        num = (nn1-nr) / PEsmpTOT
        do ip= 1, PEsmpTOT
          if (ip.le.nr) then
            SMPindex((ic-1)*PEsmpTOT+ip)= num + 1
           else
            SMPindex((ic-1)*PEsmpTOT+ip)= num
          endif
        enddo
      enddo

      do ic= 1, NCOLORtot
        do ip= 1, PEsmpTOT
          j1= (ic-1)*PEsmpTOT + ip
          j0= j1 - 1
          SMPindex(j1)= SMPindex(j0) + SMPindex(j1)
        enddo
      enddo

      allocate (SMPindex_new(0:PEsmpTOT*NCOLORtot))
      SMPindex_new(0)= 0
      do ic= 1, NCOLORtot
        do ip= 1, PEsmpTOT
          j1= (ic-1)*PEsmpTOT + ip
          j0= j1 - 1
          SMPindex_new((ip-1)*NCOLORtot+ic)= 
     &                        SMPindex(j1) - SMPindex(j0)
        enddo
      enddo

      do ip= 1, PEsmpTOT
        do ic= 1, NCOLORtot
          j1= (ip-1)*NCOLORtot + ic
          j0= j1 - 1
          SMPindex_new(j1)= SMPindex_new(j0) + SMPindex_new(j1)
        enddo
      enddo

      allocate (SMPindexG(0:PEsmpTOT))
      SMPindexG= 0
      ! nn= ICELTOT / PEsmpTOT
      ! nr= ICELTOT - nn*PEsmpTOT
      nr = mod(ICELTOT, PEsmpTOT)
      nn = (ICELTOT-nr) / PEsmpTOT
      do ip= 1, PEsmpTOT
        SMPindexG(ip)= nn
        if (ip.le.nr) SMPindexG(ip)= nn + 1
      enddo

      do ip= 1, PEsmpTOT
        SMPindexG(ip)= SMPindexG(ip-1) + SMPindexG(ip)
      enddo

      icou= 0
      do icel= 1, ICELTOT
        if (INL(icel).eq.0) icou= icou + 1
      enddo

!C
!C-- 1D array
      BLKsiz= 0
      do ip= 1, PEsmpTOT
      do ic= 1, NCOLORtot
        icNS= SMPindex_new((ip-1)*NCOLORtot + ic-1)
        ic01= SMPindex((ic-1)*PEsmpTOT + ip-1) + 1
        ic02= SMPindex((ic-1)*PEsmpTOT + ip  ) 
        icou= 0
        BLKsiz= max (BLKsiz,ic02-ic01+1)
        do k= ic01, ic02
          icel= NEWtoOLD(k)
          icou= icou + 1
          icelN= icNS + icou
          OLDtoNEWnew(icel )= icelN
          NEWtoOLDnew(icelN)= icel
        enddo
      enddo
      enddo

      write (*,'(a,i8)') 'Block Size:', BLKsiz

      allocate (BLK_SIMD_index(0:NCOLORtot))
      NBKtot= 0
      BLK_SIMD_index(0)= 0
      do ic= 1, NCOLORtot
        NBKmax= -1
      do ip= 1, PEsmpTOT
        ic01= SMPindex_new((ip-1)*NCOLORtot + ic-1)
        ic02= SMPindex_new((ip-1)*NCOLORtot + ic  )
        nnn= ic02 - ic01
        if (nnn.eq.0) then
          nnn= 0
         else
          nnn= nnn-1
        endif
        NBK= (nnn/NSIMD) + 1
        if (NBK.gt.NBKmax) then
          NBKmax= NBK
        endif
      enddo

        Ntotal= Ntotal + NBKmax*NSIMD*PEsmpTOT
        NBKtot= NBKtot + NBKmax*PEsmpTOT
        BLK_SIMD_index(ic)= BLK_SIMD_index(ic-1) + NBKmax
      enddo

      allocate (OLDtoNEWsimd(ICELTOT), NEWtoOLDsimd(0:Ntotal))
      OLDtoNEWsimd= 0
      NEWtoOLDsimd= 0

      NBKmaxALL= PEsmpTOT*NCOLORtot
      allocate (SMP_SIMD_index(0:NBKmaxALL))
      SMP_SIMD_index(0)= 0
      icou= 0
      do ic= 1, NCOLORtot
        nnbk= BLK_SIMD_index(ic) - BLK_SIMD_index(ic-1)
      do ip= 1, PEsmpTOT
        ii= (ip-1)*NCOLORtot + ic
        SMP_SIMD_index(ii)= nnbk
!        write (*,*) icou, SMP_SIMD_index(icou)
      enddo
      enddo

      do ip= 1, PEsmpTOT
      do ic= 1, NCOLORtot
        ii= (ip-1)*NCOLORtot + ic
        SMP_SIMD_index(ii)= SMP_SIMD_index(ii-1) + SMP_SIMD_index(ii)
!        write (*,*) icou, SMP_SIMD_index(icou)
      enddo
      enddo

      do ic= 1, NCOLORtot
        nnbk= BLK_SIMD_index(ic) - BLK_SIMD_index(ic-1)
      do ip= 1, PEsmpTOT
        ip1= SMPindex_new((ip-1)*NCOLORtot + ic-1) + 1
        ip2= SMPindex_new((ip-1)*NCOLORtot + ic  ) 
        iq1= NSIMD*SMP_SIMD_index((ip-1)*NCOLORtot + ic-1) + 1
        iq2= NSIMD*SMP_SIMD_index((ip-1)*NCOLORtot + ic)
!        write (*,'(10i8)') ic, ip, ip1, ip2, iq1, iq2
        do i= ip1, ip2
          ii= iq1 + (i-ip1)
          OLDtoNEWsimd(i )= ii
          NEWtoOLDsimd(ii)=  i
        enddo
      enddo
      enddo

      icou= 0
      NBKmax= 0
      do ic= 1, NCOLORtot
      do ip= 1, PEsmpTOT
        iq1= SMP_SIMD_index((ip-1)*NCOLORtot + ic-1) + 1
        iq2= SMP_SIMD_index((ip-1)*NCOLORtot + ic)
        NBKmax= max (NBKmax, iq2-iq1+1)
      do ib  = iq1, iq2
        iq0= (ib-1)*NSIMD
      do is  = 1, NSIMD
        i= iq0 + is
        if (NEWtoOLDsimd(i).eq.0) then
          icou= icou + 1
        endif
      enddo
      enddo
      enddo
      enddo
      BLKsiz= NBKmax
!      write (*,*) icou, Ntotal-ICELTOT

      icou= 0
      do i= 1, Ntotal
        if (NEWtoOLDsimd(i).eq.0) then
          icou= icou + 1
        endif
      enddo
!      write (*,*) icou, Ntotal-ICELTOT
!      read (*,*) iii

      write (*,'(a,i8)') 'Block Size:', BLKsiz

      allocate (itemL(NSIMD*BLKsiz,6,NCOLORtot*PEsmpTOT),
     &          itemU(NSIMD*BLKsiz,6,NCOLORtot*PEsmpTOT), 
     &          AL   (NSIMD*BLKsiz,6,NCOLORtot*PEsmpTOT), 
     &          AU   (NSIMD*BLKsiz,6,NCOLORtot*PEsmpTOT))
#ifdef flag_one_third
#ifdef rowunr
      if(mod(NSIMD, 3) /= 0) then
         write(*, *) 'Aribitary precision with row-wised compress, ',
     &        'C_SCS must be a multiple of three.'
         write(*, *) 'Please re-compile.'
         stop
      endif
      allocate (AL0  (NSIMD*BLKsiz/3,6,NCOLORtot*PEsmpTOT), 
     &          AU0  (NSIMD*BLKsiz/3,6,NCOLORtot*PEsmpTOT))
#elif defined colunr
      allocate (AL0  (NSIMD*BLKsiz,2,NCOLORtot*PEsmpTOT), 
     &          AU0  (NSIMD*BLKsiz,2,NCOLORtot*PEsmpTOT))
#else
      write(*, *) 'FP21, FP42 must be compiled with ',
     &                                            '-Dcolunr or -Drowunr'
      stop
#endif    
#else
      allocate (AL0  (NSIMD*BLKsiz,6,NCOLORtot*PEsmpTOT), 
     &          AU0  (NSIMD*BLKsiz,6,NCOLORtot*PEsmpTOT))
#endif

!C          itemL= 0
!C          itemU= 0
!C             AL  = 0.d0
!C             AU  = 0.d0
!C             AL0 = 0.d0
!C             AU0 = 0.d0

      nn= Ntotal + N2
      allocate (BFORCE(nn), D(nn), PHI(nn))
      if (NFLAG.eq.0) then 
         PHI   = 0.d0
         D   = 0.d0
         BFORCE= 0.d0
      endif
!C===

!C
!C +-----------------------------------+
!C | INTERIOR & NEUMANN BOUNDARY CELLs |
!C +-----------------------------------+
!C===
      S1t= omp_get_wtime()

!$omp parallel default (none)
!$omp&          shared (XYZ, VOLCEL, NEIBcell)
!$omp&          shared (itemL, itemU, AL, AU, PHI, D, BFORCE)
!$omp&          shared (INL, INU, IAL, IAU)
!$omp&          shared (AL0, AU0)
!$omp&          shared (SMPindex, SMP_SIMD_index, COND)
!$omp&          shared (NEWtoOLD, OLDtoNEW, NEWtoOLDnew, OLDtoNEWnew)
!$omp&          shared (NEWtoOLDsimd, OLDtoNEWsimd)
!$omp&         private (ic,ip,icel,ic0,icN1,icN2,icN3,icN4, icN5,icN6)
!$omp&         private (i,ib,ib0,is,ip0,iq1,iq2,iq0,ic0a)
!$omp&         private (coef,j,ii,jj,kk,js,c0,c1,EM1,EM2,EM3)
!$omp&         private (ik0,icN10,icN20,icN30,icN40, icN50,icN60)
!$omp&    firstprivate (NCOLORtot, NSIMD, NFLAG)
!$omp&    firstprivate (DX, DY, DZ, XAREA, YAREA, ZAREA)      
      
      ip = omp_get_thread_num()+1

!Iinitializing PHI, BFORCE and D      
      if (NFLAG.eq.1) then 
         do ic= 1, NCOLORtot
            iq1= SMP_SIMD_index((ip-1)*NCOLORtot + ic-1) + 1
            iq2= SMP_SIMD_index((ip-1)*NCOLORtot + ic)
            ip0= (ip-1)*NCOLORtot + ic
            iq0= (iq1-1)*NSIMD
            do ib  = iq1, iq2
               ib0= (ib-iq1)*NSIMD
               do is  = 1, NSIMD
                  i = iq0 + ib0 + is
                  PHI(i) = 0.0d0
                  D(i) = 0.0d0
                  BFORCE(i) = 0.0d0
               enddo
            enddo
         enddo
      endif
      
      do ic= 1, NCOLORtot
         iq1= SMP_SIMD_index((ip-1)*NCOLORtot + ic-1) + 1
         iq2= SMP_SIMD_index((ip-1)*NCOLORtot + ic)
         iq0= (iq1-1)*NSIMD
         ip0= (ip-1)*NCOLORtot + ic
         do ib  = iq1, iq2
            ib0= (ib-iq1)*NSIMD
            do is  = 1, NSIMD
               icel= iq0 + is + ib0
               ic0a= NEWtoOLDsimd(icel)
               if (ic0a.eq.0) then
                  D     (icel)= 1.d0
                  BFORCE(icel)= 0.d0
               else
                  ic0= NEWtoOLDnew(ic0a)
                  ik0= OLDtoNEW(ic0)
                  
                  c0= COND(ic0)
                  
                  icN10= NEIBcell(ic0,1)
                  icN20= NEIBcell(ic0,2)
                  icN30= NEIBcell(ic0,3)
                  icN40= NEIBcell(ic0,4)
                  icN50= NEIBcell(ic0,5)
                  icN60= NEIBcell(ic0,6)
                  
                  if (icN50.ne.0) then
                     c1= COND(icN50)
                     coef= ZAREA / (0.50d0*DZ/C0 + 0.50d0*DZ/C1)
                     icN5= OLDtoNEWsimd(OLDtoNEWnew(icN50))
                     D(icel)= D(icel) - coef
                     
                     if (OLDtoNEW(icN50).lt.ik0) then
                        do j= 1, INL(ik0)
                           if (IAL(j,ik0).eq.OLDtoNEW(icN50)) then
                              itemL(ib0+is,j,ip0)= icN5
                              AL(ib0+is,j,ip0)= coef
#ifndef flag_one_third
                              AL0(ib0+is,j,ip0)= coef
#endif
                              exit
                           endif
                        enddo
                     else
                        do j= 1, INU(ik0)
                           if (IAU(j,ik0).eq.OLDtoNEW(icN50)) then
                              itemU(ib0+is,j,ip0)= icN5
                              AU(ib0+is,j,ip0)= coef
#ifndef flag_one_third
                              AU0(ib0+is,j,ip0)= coef
#endif
                              exit
                           endif
                        enddo
                     endif
                  endif
                  
                  if (icN30.ne.0) then
                     c1= COND(icN30)
                     coef= YAREA / (0.50d0*DY/C0 + 0.50d0*DY/C1)
                     icN3= OLDtoNEWsimd(OLDtoNEWnew(icN30))
                     D(icel)= D(icel) - coef
                     
                     if (OLDtoNEW(icN30).lt.ik0) then
                        do j= 1, INL(ik0)
                           if (IAL(j,ik0).eq.OLDtoNEW(icN30)) then
                              itemL(ib0+is,j,ip0)= icN3
                              AL(ib0+is,j,ip0)= coef
#ifndef flag_one_third
                              AL0(ib0+is,j,ip0)= coef
#endif
                              exit
                           endif
                        enddo
                     else
                        do j= 1, INU(ik0)
                           if (IAU(j,ik0).eq.OLDtoNEW(icN30)) then
                              itemU(ib0+is,j,ip0)= icN3
                              AU(ib0+is,j,ip0)= coef
#ifndef flag_one_third
                              AU0(ib0+is,j,ip0)= coef
#endif
                              exit
                           endif
                        enddo
                     endif
                  endif
                  
                  if (icN10.ne.0) then
                     c1= COND(icN10)
                     coef= XAREA / (0.50d0*DX/C0 + 0.50d0*DX/C1)
                     icN1= OLDtoNEWsimd(OLDtoNEWnew(icN10))
                     D(icel)= D(icel) - coef
                     
                     if (OLDtoNEW(icN10).lt.ik0) then
                        do j= 1, INL(ik0)
                           if (IAL(j,ik0).eq.OLDtoNEW(icN10)) then
                              itemL(ib0+is,j,ip0)= icN1
                              AL(ib0+is,j,ip0)= coef
#ifndef flag_one_third
                              AL0(ib0+is,j,ip0)= coef
#endif
                              exit
                           endif
                        enddo
                     else
                        do j= 1, INU(ik0)
                           if (IAU(j,ik0).eq.OLDtoNEW(icN10)) then
                              itemU(ib0+is,j,ip0)= icN1
                              AU(ib0+is,j,ip0)= coef
#ifndef flag_one_third
                              AU0(ib0+is,j,ip0)= coef
#endif
                              exit
                           endif
                        enddo
                     endif
                  endif
                  
                  if (icN20.ne.0) then
                     c1= COND(icN20)
                     coef= XAREA / (0.50d0*DX/C0 + 0.50d0*DX/C1)
                     icN2= OLDtoNEWsimd(OLDtoNEWnew(icN20))
                     D(icel)= D(icel) - coef
                     
                     if (OLDtoNEW(icN20).lt.ik0) then
                        do j= 1, INL(ik0)
                           if (IAL(j,ik0).eq.OLDtoNEW(icN20)) then
                              itemL(ib0+is,j,ip0)= icN2
                              AL(ib0+is,j,ip0)= coef
#ifndef flag_one_third
                              AL0(ib0+is,j,ip0)= coef
#endif
                              exit
                           endif
                        enddo
                     else
                        do j= 1, INU(ik0)
                           if (IAU(j,ik0).eq.OLDtoNEW(icN20)) then
                              itemU(ib0+is,j,ip0)= icN2
                              AU(ib0+is,j,ip0)= coef
#ifndef flag_one_third
                              AU0(ib0+is,j,ip0)= coef
#endif
                              exit
                           endif
                        enddo
                     endif
                  endif
                  
                  if (icN40.ne.0) then
                     c1= COND(icN40)
                     coef= YAREA / (0.50d0*DY/C0 + 0.50d0*DY/C1)
                     icN4= OLDtoNEWsimd(OLDtoNEWnew(icN40))
                     D(icel)= D(icel) - coef
                     
                     if (OLDtoNEW(icN40).lt.ik0) then
                        do j= 1, INL(ik0)
                           if (IAL(j,ik0).eq.OLDtoNEW(icN40)) then
                              itemL(ib0+is,j,ip0)= icN4
                              AL(ib0+is,j,ip0)= coef
#ifndef flag_one_third
                              AL0(ib0+is,j,ip0)= coef
#endif
                              exit
                           endif
                        enddo
                     else
                        do j= 1, INU(ik0)
                           if (IAU(j,ik0).eq.OLDtoNEW(icN40)) then
                              itemU(ib0+is,j,ip0)= icN4
                              AU(ib0+is,j,ip0)= coef
#ifndef flag_one_third
                              AU0(ib0+is,j,ip0)= coef
#endif
                              exit
                           endif
                        enddo
                     endif
                  endif
                  
                  if (icN60.ne.0) then
                     c1= COND(icN60)
                     coef= ZAREA / (0.50d0*DZ/C0 + 0.50d0*DZ/C1)
                     icN6= OLDtoNEWsimd(OLDtoNEWnew(icN60))
                     D(icel)= D(icel) - coef
                     
                     if (OLDtoNEW(icN60).lt.ik0) then
                        do j= 1, INL(ik0)
                           if (IAL(j,ik0).eq.OLDtoNEW(icN60)) then
                              itemL(ib0+is,j,ip0)= icN6
                              AL(ib0+is,j,ip0)= coef
#ifndef flag_one_third
                              AL0(ib0+is,j,ip0)= coef
#endif
                              exit
                           endif
                        enddo
                     else
                        do j= 1, INU(ik0)
                           if (IAU(j,ik0).eq.OLDtoNEW(icN60)) then
                              itemU(ib0+is,j,ip0)= icN6
                              AU(ib0+is,j,ip0)= coef
#ifndef flag_one_third
                              AU0(ib0+is,j,ip0)= coef
#endif
                              exit
                           endif
                        enddo
                     endif
                  endif
                  
                  ii= XYZ(ic0,1)
                  jj= XYZ(ic0,2)
                  kk= XYZ(ic0,3)
                  
                  BFORCE(icel)= -dfloat(ii+jj+kk) * VOLCEL(ic0)
               endif
               
            enddo
         enddo
         !$omp barrier
      enddo

      do ic= 1, NCOLORtot
         iq1= SMP_SIMD_index((ip-1)*NCOLORtot + ic-1) + 1
         iq2= SMP_SIMD_index((ip-1)*NCOLORtot + ic)
         ip0= (ip-1)*NCOLORtot + ic
         iq0= (iq1-1)*NSIMD
         do ib  = iq1, iq2
            ib0= (ib-iq1)*NSIMD
#ifdef rowunr
            do is  = 1, NSIMD, 3
               js = (ib0+is-1)/3 + 1
               i = iq0 + ib0 + is 
               do k= 1, 6
                  EM1 = AL(ib0+is,   k, ip0)
                  EM2 = AL(ib0+is+1, k, ip0)
                  EM3 = AL(ib0+is+2, k, ip0)
                  AL0(js, k, ip0) =
     &                 store_ap(EM1, EM2, EM3)
                  EM1 = AU(ib0+is,   k, ip0)
                  EM2 = AU(ib0+is+1, k, ip0)
                  EM3 = AU(ib0+is+2, k, ip0)
                  AU0(js, k, ip0) =
     &                 store_ap(EM1, EM2, EM3)
               enddo
            enddo
#elif defined colunr
            do is  = 1, NSIMD
               js = ib0+is
               do k = 1, 2
                  EM1 = AL(ib0+is, k*3-2, ip0)
                  EM2 = AL(ib0+is, k*3-1, ip0)
                  EM3 = AL(ib0+is, k*3,   ip0)                  
                  AL0(ib0+is, k, ip0) =
     &                 store_ap(EM1, EM2, EM3)
                  EM1 = AU(ib0+is, k*3-2, ip0)
                  EM2 = AU(ib0+is, k*3-1, ip0)
                  EM3 = AU(ib0+is, k*3,   ip0)                  
                  AU0(ib0+is, k, ip0) =
     &                 store_ap(EM1, EM2, EM3)
               enddo
            enddo
#endif
         enddo
         !$omp barrier
      enddo

!$omp end parallel

      E1t= omp_get_wtime()
      write (*,'(1pe16.6, a)') E1t-S1t, ' sec. (assemble)'

      if (N2.ne.0) then
      icou= 0
      do ic= 1, NCOLORtot
        if (ic.eq.1) then
          kL= 0
         else if (ic.le.NCOLORtot-1) then
          kL= 3
         else if (ic.eq.NCOLORtot) then
          kL= 6
        endif

        if (ic.eq.1) then
          kU= 6
         else if (ic.le.NCOLORtot-1) then
          kU= 3
         else 
          kU= 0
        endif
      do ip= 1, PEsmpTOT
        iq1= SMP_SIMD_index((ip-1)*NCOLORtot + ic-1) + 1
        iq2= SMP_SIMD_index((ip-1)*NCOLORtot + ic)
        iq0= (iq1-1)*NSIMD
        ip0= (ip-1)*NCOLORtot + ic

        do k= 1, kL
        do ib= iq1, iq2
          ib0= (ib-iq1)*NSIMD
        do is= 1, NSIMD
          if (itemL(ib0+is,k,ip0).eq.0) then
            icou= icou + 1
            itemL(ib0+is,k,ip0)= icou + Ntotal
            if (icou.eq.N2) icou= 0
          endif
        enddo
        enddo
        enddo

        do k= 1, kU
        do ib= iq1, iq2
          ib0= (ib-iq1)*NSIMD
        do is= 1, NSIMD
          if (itemU(ib0+is,k,ip0).eq.0) then
            icou= icou + 1
            itemU(ib0+is,k,ip0)= icou + Ntotal
            if (icou.eq.N2) icou= 0
          endif
        enddo
        enddo
        enddo

      enddo
      enddo

      else

      do ic= 1, NCOLORtot
        kL= 6
        kU= 6
      do ip= 1, PEsmpTOT
        iq1= SMP_SIMD_index((ip-1)*NCOLORtot + ic-1) + 1
        iq2= SMP_SIMD_index((ip-1)*NCOLORtot + ic)
        iq0= (iq1-1)*NSIMD
        ip0= (ip-1)*NCOLORtot + ic

        do k= 1, kL
        do ib= iq1, iq2
          ib0= (ib-iq1)*NSIMD
        do is= 1, NSIMD
          if (itemL(ib0+is,k,ip0).eq.0) then
            itemL(ib0+is,k,ip0)= 1
          endif
        enddo
        enddo
        enddo

        do k= 1, kU
        do ib= iq1, iq2
          ib0= (ib-iq1)*NSIMD
        do is= 1, NSIMD
          if (itemU(ib0+is,k,ip0).eq.0) then
            itemU(ib0+is,k,ip0)= 1
          endif
        enddo
        enddo
        enddo

      enddo
      enddo

      endif
!C===

!C
!C +--------------------------+
!C | DIRICHLET BOUNDARY CELLs |
!C +--------------------------+
!C   TOP SURFACE
!C===
      do ib= 1, ZmaxCELtot
        ic0= ZmaxCEL(ib)
        coef= 2.d0 * RDZ * ZAREA
        icel= OLDtoNEWsimd(OLDtoNEWnew(ic0))
        D(icel)= D(icel) - coef
      enddo
!C===

      return
      end
