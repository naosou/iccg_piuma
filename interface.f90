#include "precision.h"

module type_c_f
  use iso_c_binding
  
  type,bind(c) :: st_CRS
     integer(c_int) N, N2, NPL, NPU, NCOLORtot, PEsmpTOT, ITR
     ! integer(c_int), allocatable :: indexL(:), itemL(:), indexU(:), itemU(:), SMPindex(:)
     ! mat_cg_f, allocatable :: D(:), AL(:), AU(:)
     ! vec_cg_f, allocatable :: B(:), X(:)
     type(c_ptr) indexL, itemL, indexU, itemU, SMPindex
     type(c_ptr) D, AL, AU
     type(c_ptr) B, X
     real(8) EPS, IER
  end type st_CRS

  type,bind(c) :: st_ELL
     integer(c_int) N, N2, NCOLORtot, PEsmpTOT, ITR
     ! integer(c_int), allocatable :: itemL(:, :), itemU(:, :), SMPindex(:)
     ! mat_cg_f, alloatble :: D(:), AL(:, :), AU(:, :)
     ! vec_cg_f, alloatble :: B(:), X(:)
     type(c_ptr) itemL, itemU, SMPindex
     type(c_ptr) D, AL, AU
     type(c_ptr) B, X
     real(8) EPS, IER
  end type st_ELL

  type,bind(c) :: st_SCS
     integer(c_int) N, N2, NCOLORtot, PEsmpTOT, ITR, BLKsiz, NSIMD
     ! integer(c_int), allocatable :: itemL(:, :, :), itemU(:, :, :), SMPindex(:)
     ! mat_cg_f, allocatable :: D(:), AL(:, :, :), AU(:, :, :)
     ! mat_precon_f, allocatable :: AL_pre(:, :, :), AU_pre(:, :, :)
     ! vec_cg_f, allocatable :: B(:), X(:)
     type(c_ptr) itemL, itemU, SMPindex
     type(c_ptr) D, AL, AU
     type(c_ptr) AL_pre, AU_pre
     type(c_ptr) B, X
     real(8) EPS, IER
  end type st_SCS

end module type_c_f

subroutine create_mat_CRS(dat_CRS) bind(c,name='create_mat_CRS')
  use type_c_f
  use PCG_CRS
  use STRUCT_CRS
  implicit none
  type(st_CRS) :: dat_CRS

  call INPUT_CRS
  call POINTER_INIT_CRS
  call BOUNDARY_CELL_CRS
  call CELL_METRICS_CRS
  call POI_GEN_CRS

  ! indexL(:) = indexL(:) - 1
  ! indexU(:) = indexU(:) - 1
  itemL(:) = itemL(:) - 1
  itemU(:) = itemU(:) - 1

  dat_CRS%N = ICELTOT
  dat_CRS%N2 = N2
  dat_CRS%NPL = NPL
  dat_CRS%NPU = NPU
  dat_CRS%NCOLORtot = NCOLORtot
  dat_CRS%PEsmpTOT = PEsmpTOT
  dat_CRS%EPS = EPSICCG
  dat_CRS%ITR = 0
  dat_CRS%IER = 0
  dat_CRS%indexL = c_loc(indexL)
  dat_CRS%indexU = c_loc(indexU)
  dat_CRS%itemL = c_loc(itemL)
  dat_CRS%itemU = c_loc(itemU)
  dat_CRS%SMPindex = c_loc(SMPindex_new)
  dat_CRS%AL = c_loc(AL)
  dat_CRS%AU = c_loc(AU)
  dat_CRS%D = c_loc(D)
  dat_CRS%B = c_loc(BFORCE)
  dat_CRS%X = c_loc(PHI)
  
end subroutine create_mat_CRS

subroutine create_mat_ELL(dat_ELL) bind(c,name='create_mat_ELL')
  use type_c_f
  use PCG_ELL
  use STRUCT_ELL
  implicit none
  type(st_ELL) :: dat_ELL

  call INPUT_ELL
  call POINTER_INIT_ELL
  call BOUNDARY_CELL_ELL
  call CELL_METRICS_ELL
  call POI_GEN_ELL

  itemL(:, :) = itemL(:, :) - 1
  itemU(:, :) = itemU(:, :) - 1
  
  dat_ELL%N = ICELTOT
  dat_ELL%N2 = N2
  dat_ELL%NCOLORtot = NCOLORtot
  dat_ELL%PEsmpTOT = PEsmpTOT
  dat_ELL%EPS = EPSICCG
  dat_ELL%ITR = 0
  dat_ELL%IER = 0
  dat_ELL%itemL = c_loc(itemL)
  dat_ELL%itemU = c_loc(itemU)
  dat_ELL%SMPindex = c_loc(SMPindex_new)
  dat_ELL%AL = c_loc(AL)
  dat_ELL%AU = c_loc(AU)
  dat_ELL%D = c_loc(D)
  dat_ELL%B = c_loc(BFORCE)
  dat_ELL%X = c_loc(PHI)

end subroutine create_mat_ELL

subroutine create_mat_SCS(dat_SCS) bind(c,name='create_mat_SCS')
  use type_c_f
  use PCG_SCS
  use STRUCT_SCS
  implicit none
  type(st_SCS) :: dat_SCS

  call INPUT_SCS
  call POINTER_INIT_SCS
  call BOUNDARY_CELL_SCS
  call CELL_METRICS_SCS
  call POI_GEN_SCS

  itemL(:, :, :) = itemL(:, :, :) - 1
  itemU(:, :, :) = itemU(:, :, :) - 1

  dat_SCS%N = Ntotal
  dat_SCS%N2 = N2
  dat_SCS%NCOLORtot = NCOLORtot
  dat_SCS%PEsmpTOT = PEsmpTOT
  dat_SCS%EPS = EPSICCG
  dat_SCS%ITR = 0
  dat_SCS%IER = 0
  dat_SCS%BLKsiz = BLKsiz
  dat_SCS%NSIMD = NSIMD
  dat_SCS%itemL = c_loc(itemL)
  dat_SCS%itemU = c_loc(itemU)
  dat_SCS%SMPindex = c_loc(SMP_SIMD_index)
  dat_SCS%AL = c_loc(AL)
  dat_SCS%AU = c_loc(AU)
  dat_SCS%AL_pre = c_loc(AL0)
  dat_SCS%AU_pre = c_loc(AU0)
  dat_SCS%D = c_loc(D)
  dat_SCS%B = c_loc(BFORCE)
  dat_SCS%X = c_loc(PHI)

end subroutine create_mat_SCS
