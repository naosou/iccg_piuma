# SYSTEM := INTEL
SYSTEM := GNU
# SYSTEM:=INTELdebug
# SYSTEM:=GNUdebug

#intel
ifeq ($(SYSTEM),INTEL)
   FC:=ifort
   CC:=icc
   LINK:=ifort
   OPTFLAGS  := -fpp -align array64byte -O3 -xHost -qopenmp -ipo 
   OPTFLAGSF90 := -fpp -O3 -xHost -qopenmp -align array64byte -ipo -stand f03 -diag-disable 5268
   OPTFLAGSC := -O3 -xHost -qopenmp -ipo
   FFLAGS   := $(OPTFLAGS)
   F90FLAGS := $(OPTFLAGSF90)
   CFLAGS   := $(OPTFLAGSC)
   LNKFLAGS  := $(OPTFLAGS)
endif

#intel debug
ifeq ($(SYSTEM),INTELdebug)
   FC:=ifort
   CC:=icc
   LINK:=ifort
   DEBUGFLAGS := -fpp -g -traceback -check all -qopenmp
   DEBUGFLAGSF90 := -fpp -g -traceback -check all -qopenmp -stand f03 -diag-disable 5268   
   DEBUGFLAGSC := -g -O0 -qopenmp
   FFLAGS   := $(DEBUGFLAGS)
   F90FLAGS := $(DEBUGFLAGSF90)
   CFLAGS   := $(DEBUGFLAGSC)
   LNKFLAGS := $(OPTFLAGS)
endif

#GNU
ifeq ($(SYSTEM),GNU)
   FC:=gfortran
   CC:=gcc
   LINK:=gfortran
   OPTFLAGS := -cpp -O3 -fopenmp -flto -march=native
   OPTFLAGSF90 := -cpp -O3 -fopenmp -ffree-line-length-none -std=f2003 -flto -march=native
   OPTFLAGSC := -O3 -fopenmp -flto -march=native -fargument-noalias -fno-strict-aliasing
   FFLAGS   := $(OPTFLAGS)
   F90FLAGS := $(OPTFLAGSF90)
   CFLAGS   := $(OPTFLAGSC)
   LNKFLAGS := $(OPTFLAGS)
endif

#GNU_debug
ifeq ($(SYSTEM),GNUdebug)
   FC:=gfortran
   CC:=gcc
   LINK:=gfortran
   DEBUGFLAGS := -cpp -fbounds-check -O0 -Wuninitialized -fbacktrace -g -fopenmp
   DEBUGFLAGSF90 := -cpp -fbounds-check -O0 -fopenmp -ffpe-trap=invalid,zero,overflow -Wuninitialized -fbacktrace -g -std=f2003 -ffree-line-length-none -fall-intrinsics
   DEBUGFLAGSC := -g -O0 -fopenmp
   FFLAGS   := $(DEBUGFLAGS)
   F90FLAGS := $(DEBUGFLAGSF90)
   CFLAGS   := $(DEBUGFLAGSC)
   LNKFLAGS := $(DEBUGFLAGS)
endif

OBJs_COMMON :=  mc.o cm.o rcm.o cmrcm.o interface.o main.o CRS.o ELL.o SCS.o
OBJs_CRS    :=  pcg_crs.o struct_crs.o input_crs.o cell_metrics_crs.o pointer_init_crs.o boundary_cell_crs.o poi_gen_crs.o
OBJs_ELL    :=	pcg_ell.o struct_ell.o input_ell.o cell_metrics_ell.o pointer_init_ell.o boundary_cell_ell.o poi_gen_ell.o
OBJs_SCS    :=	pcg_scs.o struct_scs.o input_scs.o cell_metrics_scs.o pointer_init_scs.o boundary_cell_scs.o poi_gen_scs.o

default	: GW3D-FVM

.SUFFIXES:
.SUFFIXES: .f .f90 .c .o

GW3D-FVM : $(OBJs_CRS) $(OBJs_ELL) $(OBJs_SCS) $(OBJs_COMMON) 
	$(LINK) -o $@ $(LNKFLAGS) $^

.f.o   :
	$(FC) -c $(FFLAGS) $<

.f90.o :
	$(FC) -c $(F90FLAGS) $<

.c.o   :
	$(CC) -c $(CFLAGS) $<

.PHONY: clean
clean:
	rm $(OBJs_COMMON) $(OBJs_CRS) $(OBJs_ELL) $(OBJs_SCS)
