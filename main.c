//decrement itemL, itemU
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "precision.h"

#define CHUNK 128

#define type_CRS 0
#define type_ELL 1
#define type_SCS 2

typedef struct{
  int N, N2, NPL, NPU, NCOLORtot, PEsmpTOT, ITR;
  int *indexL, *itemL, *indexU, *itemU, *SMPindex;
  mat_cg *D, *AL, *AU;
  vec_cg *B, *X;
  double EPS, IER;
}st_CRS;

typedef struct{
  int N, N2, NCOLORtot, PEsmpTOT, ITR;
  int **itemL, **itemU, *SMPindex;
  mat_cg *D, **AL, **AU;
  vec_cg *B, *X;
  double EPS, IER;
}st_ELL;

typedef struct{
  int N, N2, NCOLORtot, PEsmpTOT, ITR, BLKsiz, NSIMD;
  int ***itemL, ***itemU, *SMPindex;
  mat_cg *D, ***AL, ***AU;
  mat_precon ***AL_pre, ***AU_pre;
  vec_cg *B, *X;
  double EPS, IER;
}st_SCS;

void create_mat_CRS(st_CRS *dat_CRS); 
void create_mat_ELL(st_ELL *dat_ELL); 
void create_mat_SCS(st_SCS *dat_SCS); 

void solver_ICCG_CRS(int N, int N2, int NPL, int NPU, int *indexL, int *itemL, int *indexU, int *itemU,
		     mat_cg *D, vec_cg *B, vec_cg *X, mat_cg *AL, mat_cg *AU,
		     int NCOLORtot, int PEsmpTOT, int *SMPindex, double EPS, int ITR, double IER);
void solver_ICCG_ELL(int N, int N2, int **itemL, int **itemU, mat_cg *D, vec_cg *B, vec_cg *X, mat_cg **AL, mat_cg **AU,
		     int NCOLORtot, int PEsmpTOT, int *SMPindex, double EPS, int ITR, double IER);
void solver_ICCG_SCS(int N, int N2, int BLKsiz, int NCOLORtot, int PEsmpTOT, int ***itemL, int ***itemU, mat_cg *D, vec_cg *B, vec_cg *X,
		     mat_cg ***AL, mat_cg ***AU, mat_precon ***AL_pre, mat_precon ***AU_pre, int *SMPindex,
		     double EPS, int ITR, double IER, int NSIMD);

int main(int argc, char *argv[])
{
  int type;
  char *name_type;
  st_CRS dat_CRS;
  st_ELL dat_ELL;
  st_SCS dat_SCS;
  
  if(argc != 2){
    printf("Required input file\n");
    exit(1);
  }
  name_type = argv[1];
  
  if(strcmp(name_type, "CRS") == 0){
    printf("Storage format is CRS.\n");
    type = type_CRS;
  }else if(strcmp(name_type, "ELL") == 0){
    printf("Storage format is ELL.\n");
    type = type_ELL;
  }else if(strcmp(name_type, "SCS") == 0){
    printf("Storage format is SCS.\n");
    type = type_SCS;	    
  }else{
    printf("Invalid matrix format type\n");
    exit(1);
  }

  switch(type){
  case type_CRS:
    create_mat_CRS(&dat_CRS);
    solver_ICCG_CRS(dat_CRS.N, dat_CRS.N2, dat_CRS.NPL, dat_CRS.NPU, dat_CRS.indexL, dat_CRS.itemL, dat_CRS.indexU,
		    dat_CRS.itemU, dat_CRS.D, dat_CRS.B, dat_CRS.X, dat_CRS.AL, dat_CRS.AU, dat_CRS.NCOLORtot,
		    dat_CRS.PEsmpTOT, dat_CRS.SMPindex, dat_CRS.EPS, dat_CRS.ITR, dat_CRS.IER);
    break;
  case type_ELL:
    create_mat_ELL(&dat_ELL);
    solver_ICCG_ELL(dat_ELL.N, dat_ELL.N2, dat_ELL.itemL, dat_ELL.itemU, dat_ELL.D, dat_ELL.B, dat_ELL.X,
		    dat_ELL.AL, dat_ELL.AU, dat_ELL.NCOLORtot, dat_ELL.PEsmpTOT, dat_ELL.SMPindex,
		    dat_ELL.EPS, dat_ELL.ITR, dat_ELL.IER);
    break;
  case type_SCS:
    create_mat_SCS(&dat_SCS);
    /* solver_ICCG_SCS(dat_SCS.N, dat_SCS.N2, dat_SCS.BLKsiz, dat_SCS.itemL, dat_SCS.itemU, dat_SCS.D, dat_SCS.B, */
    /* 		    dat_SCS.X, dat_SCS.AL, dat_SCS.AU, dat_SCS.AL_pre, dat_SCS.AU_pre, */
    /* 		    dat_SCS.NCOLORtot, dat_SCS.PEsmpTOT, dat_SCS.SMPindex, dat_SCS.EPS, dat_SCS.ITR, */
    /* 		    dat_SCS.IER, dat_SCS.NSIMD); */
    solver_ICCG_SCS(dat_SCS.N, dat_SCS.N2, dat_SCS.BLKsiz, dat_SCS.NCOLORtot, dat_SCS.PEsmpTOT,
		    dat_SCS.itemL, dat_SCS.itemU, dat_SCS.D, dat_SCS.B,
		    dat_SCS.X, dat_SCS.AL, dat_SCS.AU, dat_SCS.AL_pre, dat_SCS.AU_pre,
		    dat_SCS.SMPindex, dat_SCS.EPS, dat_SCS.ITR,
		    dat_SCS.IER, dat_SCS.NSIMD);
    break;
  default :
    printf("Invalid file format\n");
    exit(1);
  }
  
  return 0;
}
