!C
!C***
!C*** POI_GEN
!C***
!C
!C    generate COEF. MATRIX for POISSON equations
!C    
      subroutine POI_GEN_CRS

      use STRUCT_CRS
      use PCG_CRS

      implicit REAL*8 (A-H,O-Z)

!C
!C-- INIT.
      nn = ICELTOT

      NL= 6
      NU= 6

      allocate (BFORCE(nn), D(nn), PHI(nn))
      allocate (INL(nn), INU(nn), IAL(NL,nn), IAU(NU,nn))

      INL= 0
      INU= 0
      IAL= 0
      IAU= 0

!C
!C-- INTERIOR & NEUMANN boundary's
      do icel= 1, ICELTOT
        icN1= NEIBcell(icel,1)
        icN2= NEIBcell(icel,2)
        icN3= NEIBcell(icel,3)
        icN4= NEIBcell(icel,4)
        icN5= NEIBcell(icel,5)
        icN6= NEIBcell(icel,6)

        if (icN5.ne.0) then
          icou= INL(icel) + 1
          IAL(icou,icel)= icN5
          INL(     icel)= icou
        endif

        if (icN3.ne.0) then
          icou= INL(icel) + 1
          IAL(icou,icel)= icN3
          INL(     icel)= icou
        endif

        if (icN1.ne.0) then
          icou= INL(icel) + 1
          IAL(icou,icel)= icN1
          INL(     icel)= icou
        endif

        if (icN2.ne.0) then
          icou= INU(icel) + 1
          IAU(icou,icel)= icN2
          INU(     icel)= icou
        endif

        if (icN4.ne.0) then
          icou= INU(icel) + 1
          IAU(icou,icel)= icN4
          INU(     icel)= icou
        endif

        if (icN6.ne.0) then
          icou= INU(icel) + 1
          IAU(icou,icel)= icN6
          INU(     icel)= icou
        endif

      enddo
!C===

!C
!C +---------------+
!C | MULTICOLORING |
!C +---------------+
!C===
      allocate (OLDtoNEW(ICELTOT), NEWtoOLD(ICELTOT))
      allocate (OLDtoNEWnew(ICELTOT), NEWtoOLDnew(ICELTOT))
      allocate (COLORindex(0:ICELTOT))

 111    continue
        write (*,'(//a,i8,a)') 'You have', ICELTOT, ' elements.'
        write (*,'(  a     )') 'How many colors do you need ?'
        write (*,'(  a     )') '  #COLOR must be more than 2 and'
        write (*,'(  a,i8  )') '  #COLOR must not be more than', ICELTOT
        write (*,'(  a     )') '   CM if #COLOR .eq. 0'
        write (*,'(  a     )') '  RCM if #COLOR .eq.-1'
        write (*,'(  a     )') 'CMRCM if #COLOR .le.-2'
        write (*,'(  a     )') '=>'

      if (NCOLORtot.eq.1.or.NCOLORtot.gt.ICELTOT) goto 111

      if (NCOLORtot.gt.0) then
        call MC  (ICELTOT, NL, NU, INL, IAL, INU, IAU,                  &
     &            NCOLORtot, COLORindex, NEWtoOLD, OLDtoNEW)
	write (*,'(//a)') '###  MultiColoring' 
      endif

      if (NCOLORtot.eq.0) then
        call  CM (ICELTOT, NL, NU, INL, IAL, INU, IAU,                  &
     &            NCOLORtot, COLORindex, NEWtoOLD, OLDtoNEW)
	write (*,'(//a)') '###  CM' 
      endif

      if (NCOLORtot.eq.-1) then
        call RCM (ICELTOT, NL, NU, INL, IAL, INU, IAU,                  &
     &            NCOLORtot, COLORindex, NEWtoOLD, OLDtoNEW)
	write (*,'(//a)') '### RCM' 
      endif

      if (NCOLORtot.lt.-1) then
        call CMRCM (ICELTOT, NL, NU, INL, IAL, INU, IAU,                &
     &            NCOLORtot, COLORindex, NEWtoOLD, OLDtoNEW)
	write (*,'(//a)') '### CM-RCM' 
      endif

      write (*,'(//a,i8,// )') '### FINAL COLOR NUMBER', NCOLORtot
      allocate (SMPindex(0:PEsmpTOT*NCOLORtot))
      SMPindex= 0
      do ic= 1, NCOLORtot
        nn1= COLORindex(ic) - COLORindex(ic-1)
        ! num= nn1 / PEsmpTOT
        ! nr = nn1 - PEsmpTOT*num
        nr = mod(nn1, PEsmpTOT)
        num = (nn1-nr) / PEsmpTOT
        do ip= 1, PEsmpTOT
          if (ip.le.nr) then
            SMPindex((ic-1)*PEsmpTOT+ip)= num + 1
           else
            SMPindex((ic-1)*PEsmpTOT+ip)= num
          endif
        enddo
      enddo

      allocate (SMPindex_new(0:PEsmpTOT*NCOLORtot))
      SMPindex_new(0)= 0
      do ic= 1, NCOLORtot
        do ip= 1, PEsmpTOT
          j1= (ic-1)*PEsmpTOT + ip
          j0= j1 - 1
          SMPindex_new((ip-1)*NCOLORtot+ic)= SMPindex(j1)
          SMPindex(j1)= SMPindex(j0) + SMPindex(j1)
        enddo
      enddo

      do ip= 1, PEsmpTOT
        do ic= 1, NCOLORtot
          j1= (ip-1)*NCOLORtot + ic
         j0= j1 - 1
          SMPindex_new(j1)= SMPindex_new(j0) + SMPindex_new(j1)
        enddo
      enddo

      allocate (SMPindexG(0:PEsmpTOT))
      SMPindexG= 0
      ! nn= ICELTOT / PEsmpTOT
      ! nr= ICELTOT - nn*PEsmpTOT
      nr = mod(ICELTOT, PEsmpTOT)
      nn = (ICELTOT-nr) / PEsmpTOT
      do ip= 1, PEsmpTOT
        SMPindexG(ip)= nn
        if (ip.le.nr) SMPindexG(ip)= nn + 1
      enddo

!C
!C-- ARRAY init.
      if (NFLAG.eq.0) then 
        BFORCE= 0.d0
        PHI   = 0.d0
        D     = 0.d0
        OLDtoNEWnew= 0
        NEWtoOLDnew= 0
       else
!$omp parallel do private (ip,icel) 
        do ip= 1, PEsmpTOT
          do icel= SMPindex_new((ip-1)*NCOLORtot+1), 
     &             SMPindex_new(ip*NCOLORtot)
            BFORCE(icel)= 0.d0
            PHI   (icel)= 0.d0
            D(icel)     = 0.d0
            OLDtoNEWnew(icel)= 0
            NEWtoOLDnew(icel)= 0
          enddo
        enddo
!$omp end parallel do
      endif

      do ip= 1, PEsmpTOT
      do ic= 1, NCOLORtot
        icNS= SMPindex_new((ip-1)*NCOLORtot + ic-1)
        ic01= SMPindex((ic-1)*PEsmpTOT + ip-1) + 1
        ic02= SMPindex((ic-1)*PEsmpTOT + ip  ) 
        icou= 0
        do k= ic01, ic02
          icel= NEWtoOLD(k)
          icou= icou + 1
          icelN= icNS + icou
          OLDtoNEWnew(icel )= icelN
          NEWtoOLDnew(icelN)= icel
        enddo
      enddo
      enddo

      do ip= 1, PEsmpTOT
        SMPindexG(ip)= SMPindexG(ip-1) + SMPindexG(ip)
      enddo

      nn = ICELTOT
      allocate (indexL(0:nn), indexU(0:nn))
      indexL(0)= 0
      indexU(0)= 0

      if (NFLAG.eq.0) then
        do icel= 1, ICELTOT
          indexL(icel)= 0
          indexU(icel)= 0
        enddo
       else
!$omp parallel do private (ip,icel,k) 
        do ip= 1, PEsmpTOT
          do icel= SMPindex_new((ip-1)*NCOLORtot+1), 
     &             SMPindex_new(ip*NCOLORtot)
            indexL(icel)= 0
            indexU(icel)= 0
          enddo
        enddo
      endif

!$omp parallel do private (ip,icel,ic0,ik0) 
      do ip  = 1, PEsmpTOT
      do icel= SMPindex((ip-1)*NCOLORtot)+1, SMPindex(ip*NCOLORtot)
        ic0 = NEWtoOLDnew(icel)
        ik0 = OLDtoNEW(ic0)
        indexL(icel)= INL(ik0)
        indexU(icel)= INU(ik0)
      enddo
      enddo

      do icel= 1, ICELTOT
        indexL(icel)= indexL(icel) + indexL(icel-1)
        indexU(icel)= indexU(icel) + indexU(icel-1)
      enddo

      NPL= indexL(ICELTOT)
      NPU= indexU(ICELTOT)

      allocate (itemL(NPL), AL(NPL))
      allocate (itemU(NPU), AU(NPU))

      if (NFLAG.eq.0) then 
        itemL= 0
        itemU= 0
           AL= 0.d0
           AU= 0.d0
       else
!$omp parallel do private (ip,icel,k) 
        do ip= 1, PEsmpTOT
          do icel= SMPindex_new((ip-1)*NCOLORtot+1), 
     &             SMPindex_new(ip*NCOLORtot)
            do k= indexL(icel-1)+1, indexL(icel)
              itemL(k)= 0
                 AL(k)= 0.d0
            enddo
            do k= indexU(icel-1)+1, indexU(icel)
              itemU(k)= 0
                 AU(k)= 0.d0
            enddo
          enddo
        enddo
!$omp end parallel do
      endif

      icou= 0
      do icel= 1, ICELTOT
        if (INL(icel).eq.0) icou= icou + 1
      enddo
      write (*,'(/a,i10/)') "incompatible nodes", icou 
!C===

!C
!C +-----------------------------------+
!C | INTERIOR & NEUMANN BOUNDARY CELLs |
!C +-----------------------------------+
!C===
      S1t= omp_get_wtime()
!$omp parallel do private (ip,icel,ic0,icN1,icN2,icN3,icN4, icN5,icN6)  &
!$omp&            private (coef,j,ii,jj,kk)                             &
!$omp&            private (ik0,icN10,icN20,icN30,icN40, icN50,icN60)  
      do ip  = 1, PEsmpTOT
      do icel= SMPindex((ip-1)*NCOLORtot)+1, SMPindex(ip*NCOLORtot)
        ic0 = NEWtoOLDnew(icel)
        ik0 = OLDtoNEW(ic0)

        icN10= NEIBcell(ic0,1)
        icN20= NEIBcell(ic0,2)
        icN30= NEIBcell(ic0,3)
        icN40= NEIBcell(ic0,4)
        icN50= NEIBcell(ic0,5)
        icN60= NEIBcell(ic0,6)

        if (icN50.ne.0) then
          icN5= OLDtoNEW(icN50)
          coef= RDZ * ZAREA
          D(icel)= D(icel) - coef

          if (icN5.lt.ik0) then
            do j= 1, INL(ik0)
              if (IAL(j,ik0).eq.icN5) then
                itemL(j+indexL(icel-1))= OLDtoNEWnew(icN50)
                   AL(j+indexL(icel-1))= coef
                exit
              endif
            enddo
           else
            do j= 1, INU(ik0)
              if (IAU(j,ik0).eq.icN5) then
                itemU(j+indexU(icel-1))= OLDtoNEWnew(icN50)
                   AU(j+indexU(icel-1))= coef
                exit
              endif
            enddo
          endif
        endif

        if (icN30.ne.0) then
          icN3= OLDtoNEW(icN30)
          coef= RDZ * ZAREA
          D(icel)= D(icel) - coef

          if (icN3.lt.ik0) then
            do j= 1, INL(ik0)
              if (IAL(j,ik0).eq.icN3) then
                itemL(j+indexL(icel-1))= OLDtoNEWnew(icN30)
                   AL(j+indexL(icel-1))= coef
                exit
              endif
            enddo
           else
            do j= 1, INU(ik0)
              if (IAU(j,ik0).eq.icN3) then
                itemU(j+indexU(icel-1))= OLDtoNEWnew(icN30)
                   AU(j+indexU(icel-1))= coef
                exit
              endif
            enddo
          endif
        endif

        if (icN10.ne.0) then
          icN1= OLDtoNEW(icN10)
          coef= RDZ * ZAREA
          D(icel)= D(icel) - coef

          if (icN1.lt.ik0) then
            do j= 1, INL(ik0)
              if (IAL(j,ik0).eq.icN1) then
                itemL(j+indexL(icel-1))= OLDtoNEWnew(icN10)
                   AL(j+indexL(icel-1))= coef
                exit
              endif
            enddo
           else
            do j= 1, INU(ik0)
              if (IAU(j,ik0).eq.icN1) then
                itemU(j+indexU(icel-1))= OLDtoNEWnew(icN10)
                   AU(j+indexU(icel-1))= coef
                exit
              endif
            enddo
          endif
        endif

        if (icN20.ne.0) then
          icN2= OLDtoNEW(icN20)
          coef= RDZ * ZAREA
          D(icel)= D(icel) - coef

          if (icN2.lt.ik0) then
            do j= 1, INL(ik0)
              if (IAL(j,ik0).eq.icN2) then
                itemL(j+indexL(icel-1))= OLDtoNEWnew(icN20)
                   AL(j+indexL(icel-1))= coef
                exit
              endif
            enddo
           else
            do j= 1, INU(ik0)
              if (IAU(j,ik0).eq.icN2) then
                itemU(j+indexU(icel-1))= OLDtoNEWnew(icN20)
                   AU(j+indexU(icel-1))= coef
                exit
              endif
            enddo
          endif
        endif

        if (icN40.ne.0) then
          icN4= OLDtoNEW(icN40)
          coef= RDZ * ZAREA
          D(icel)= D(icel) - coef

          if (icN4.lt.ik0) then
            do j= 1, INL(ik0)
              if (IAL(j,ik0).eq.icN4) then
                itemL(j+indexL(icel-1))= OLDtoNEWnew(icN40)
                   AL(j+indexL(icel-1))= coef
                exit
              endif
            enddo
           else
            do j= 1, INU(ik0)
              if (IAU(j,ik0).eq.icN4) then
                itemU(j+indexU(icel-1))= OLDtoNEWnew(icN40)
                   AU(j+indexU(icel-1))= coef
                exit
              endif
            enddo
          endif
        endif

        if (icN60.ne.0) then
          icN6= OLDtoNEW(icN60)
          coef= RDZ * ZAREA
          D(icel)= D(icel) - coef

          if (icN6.lt.ik0) then
            do j= 1, INL(ik0)
              if (IAL(j,ik0).eq.icN6) then
                itemL(j+indexL(icel-1))= OLDtoNEWnew(icN60)
                   AL(j+indexL(icel-1))= coef
                exit
              endif
            enddo
           else
            do j= 1, INU(ik0)
              if (IAU(j,ik0).eq.icN6) then
                itemU(j+indexU(icel-1))= OLDtoNEWnew(icN60)
                   AU(j+indexU(icel-1))= coef
                exit
              endif
            enddo
          endif
        endif

        ii= XYZ(ic0,1)
        jj= XYZ(ic0,2)
        kk= XYZ(ic0,3)

        BFORCE(icel)= -dfloat(ii+jj+kk) * VOLCEL(ic0)

      enddo
      enddo
!$omp end parallel do

      E1t= omp_get_wtime()
      write (*,'(1pe16.6, a)') E1t-S1t, ' sec. (assemble)'
!C===

!C
!C +--------------------------+
!C | DIRICHLET BOUNDARY CELLs |
!C +--------------------------+
!C   TOP SURFACE
!C===
      do ib= 1, ZmaxCELtot
        ic0= ZmaxCEL(ib)
        coef= 2.d0 * RDZ * ZAREA
        icel= OLDtoNEWnew(ic0)
        D(icel)= D(icel) - coef
      enddo
!C===

      return
      end
