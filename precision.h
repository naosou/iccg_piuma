#ifdef vec_precon_single
#define vec_precon float
#define temp_precon float
#define vec_precon_f real(c_float)
#define temp_precon_f real(c_float)
#define vec_precon_bit 32
#define name_vec_precon "single"

#elif vec_precon_half
#define vec_precon half
#define temp_precon half
#define vec_precon_f real(c_half)
#define temp_precon_f real(c_half)
#define vec_precon_bit 16
#define name_vec_precon "half"

#else
#define vec_precon double
#define temp_precon double
#define vec_precon_f real(c_double)
#define temp_precon_f real(c_double)
#define vec_precon_bit 64
#define name_vec_precon "double"
#endif

#ifdef mat_precon_fp42
#include "libdafpp_fix_f.h"
#define mat_precon fp42x3
#define mat_precon_bit 42
#define temp_mat_precon double
#define temp_mat_precon_f real(c_double)
#define ccp 4
#define name_mat_precon "fp42"
#define flag_one_third
#define load_ap fp42x3_to_floatx3_f
#define store_ap floatx3_to_fp42x3_f

#elif mat_precon_single
#define mat_precon float
#define mat_precon_f real(c_float)
#define mat_precon_bit 32
#define temp_mat_precon float
#define temp_mat_precon_f real(c_float)
#define ccp 4
#define name_mat_precon "single"

#elif mat_precon_fp21
#include "libdafpp_fix_f.h"
#define mat_precon fp21x3
#define mat_precon_bit 21
#define temp_mat_precon float
#define temp_mat_precon_f real(c_float)
#define ccp 4
#define name_mat_precon "fp21"
#define flag_one_third
#define load_ap fp21x3_to_floatx3_loc_f
#define store_ap floatx3_to_fp21x3_f

#elif mat_precon_half
#define mat_precon half
#define mat_precon_f real(c_half)
#define mat_precon_bit 16
#define temp_mat_precon half
#define temp_mat_precon_f real(c_half)
#define ccp 2
#define name_mat_precon "half"

#else
#define mat_precon double
#define mat_precon_f real(c_double)
#define mat_precon_bit 64
#define temp_mat_precon double
#define temp_mat_precon_f real(c_double)
#define ccp 8
#define name_mat_precon "double"
#endif


#define vec_cg double
#define vec_cg_f real(c_double)
#define vec_cg_bit 64
#define mat_cg double 
#define mat_cg_f real(c_double) 

#define kint 4
