#include<stdlib.h>
#include<stdio.h>

#include <omp.h>

#include "math.h"

#include "precision.h"

#if vec_precon_bit == vec_cg_bit
#define TGT_PRECON W
#else
#define TGT_PRECON WZ
#define flag_prepost
#endif

void solver_ICCG_ELL(int N, int N2, int itemL[6][N], int itemU[6][N], mat_cg *D, vec_cg *B, vec_cg *X, mat_cg AL[6][N], mat_cg AU[6][N],
		     int NCOLORtot, int PEsmpTOT, int *SMPindex, double EPS, int ITR, double IER)
{
  mat_precon *Ws_DD;
  vec_precon *ZD;
  
  int R = 1, Z = 0, Q = 0, P = 2, DD = 3;

  double W_RHO[PEsmpTOT], W_C1[PEsmpTOT], W_DNRM2[PEsmpTOT];
  
  vec_precon VAL_p, SW_p;
  vec_cg VAL;

  int nth, ls, rmin;

  int i, ii, k, L, is, js, ib, ib0, ic, ip, ip0, iq0, ip1, iq1, iq2;
  vec_cg C1, ALPHA, BETA, RHO, RHO1, ERR, ERR2, BNRM2, DNRM2;

  double Stime, Etime, time_precon = 0.0;

  char nvp[10] = name_vec_precon, nmp[10]  = name_mat_precon;

  FILE *fp;
//
// +------+
// | INIT |
// +------+
//===

  printf("solver_ICCG_ELL, %s-%s\n", nmp, nvp);
  
  vec_cg (*W)[N+N2];
  if((W = (double(*)[N+N2])malloc((N+N2) * 4 * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  if((Ws_DD = (double *)malloc((N+N2) * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  vec_cg (*WZ)[1];
#ifdef flag_prepost
  if((WZ = (double(*)[1])malloc((N+N2) * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
#endif
  mat_precon (*AL_pre)[N+N2];
  if((AL_pre = (double (*)[N+N2])malloc(6 * (N+N2) * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  mat_precon (*AU_pre)[N+N2];
  if((AU_pre = (double (*)[N+N2])malloc(6 * (N+N2) * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  if((ZD = (double *)malloc((N+N2) * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }

#pragma omp parallel private(ip,i)
  {
    ip = omp_get_thread_num();
    for(i=SMPindex[ip*NCOLORtot]; i < SMPindex[(ip+1)*NCOLORtot]; i++){
      X[i]    = 0.0;
      W[1][i] = 0.0;
      W[2][i] = 0.0;
      W[3][i] = 0.0;
      ZD[i]   = 0.0;
#ifdef flag_prepost
      WZ[Z][i] = 0.0;
#endif
    }
  }

#pragma omp parallel default(none) \
  private(ip,ip1,i,VAL,k,ic) \
  shared(Ws_DD,AL,D,itemL,SMPindex)  \
  firstprivate(NCOLORtot)
  {
    ip = omp_get_thread_num();
    for(ic=0; ic<NCOLORtot; ic++){
      ip1= ip*NCOLORtot + ic;
	if (ic == 0){
	  for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	    Ws_DD[i]= 1.0/D[i];
	}else if (ic < NCOLORtot-1){
	  for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	    Ws_DD[i]= D[i] - (AL[0][i]*AL[0][i]) * Ws_DD[itemL[0][i]];
	  for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	    Ws_DD[i]= Ws_DD[i] - (AL[1][i]*AL[1][i]) * Ws_DD[itemL[1][i]];
	  for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	    Ws_DD[i]= 1.0/(Ws_DD[i] - (AL[2][i]*AL[2][i]) * Ws_DD[itemL[2][i]]);
	}else{
	  for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	    Ws_DD[i]= D[i] - (AL[0][i]*AL[0][i]) * Ws_DD[itemL[0][i]];
	  for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	    for(k=1;k<5;k++)
	      Ws_DD[i]= Ws_DD[i] - (AL[k][i]*AL[k][i]) * Ws_DD[itemL[k][i]];
	    for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	      Ws_DD[i]= 1.0/(Ws_DD[i] - (AL[5][i]*AL[5][i]) * Ws_DD[itemL[5][i]]);
	}
#pragma omp barrier
    }
  }


#pragma omp parallel default(none) \
        shared(AL, AU, AL_pre, AU_pre, SMPindex) \
        private(ic, i, k, ip, ip1) \
        firstprivate(NCOLORtot)
  {
    ip = omp_get_thread_num();
    for(ic = 0; ic < NCOLORtot; ic++){
      ip1 = ip*NCOLORtot + ic;
      for(i = SMPindex[ip1]; i< SMPindex[ip1+1]; i++)
	for(k = 0; k< 6; k++){
	  AL_pre[k][i] = AL[k][i];
	  AU_pre[k][i] = AU[k][i];
	}
    }
  }

//
// +-----------------------+
// | {r0}= {b} - [A]{xini} |
// +-----------------------+
//===
  BNRM2= 0.0;
#pragma omp parallel for private(i) reduction(+:BNRM2)
  for(i = 0; i < N; i++){
    W[R][i]= B[i];
    BNRM2 = BNRM2 + B[i] * B[i];
  }
//====================
  Stime= omp_get_wtime();
//
//***************************************************************  ITERATION
  ITR = N;

#pragma omp parallel default(none)					\
  shared(B, X, D, AL, AU, AL_pre, AU_pre, W, Ws_DD, itemL, itemU,	\
	 WZ, ZD, W_RHO, W_C1, W_DNRM2, SMPindex, fp,			\
	 time_precon, RHO1, IER, ITR, ERR2)				\
  private(ic,ip,ip0,ip1,iq0,iq1,iq2,ib,ib0,is,i,k,rmin,L,		\
	  nth,ls,VAL,VAL_p,SW_p,RHO,DNRM2,C1,ERR,ALPHA,BETA)		\
  firstprivate(N, NCOLORtot, BNRM2, EPS, R, Z, Q, P, DD)
  {
    ip = omp_get_thread_num();
    nth= omp_get_num_threads();
    ls = (N+nth-1)/nth;
    rmin = (ip+1)*ls<N ? (ip+1)*ls : N;
    // printf("Check range ip = %d, min = %d, max = %d\n", ip, ip*ls, rmin);
    
    for(L=0; L<ITR; L++)
      {
//
// +----------------+
// | {z}= [Minv]{r} |
// +----------------+
//===      

#pragma omp simd
	for(i = ip*ls; i < rmin; i++){
	  TGT_PRECON[Z][i] = W[R][i];
	  ZD[i]= 0.0;
	}
#pragma omp barrier
	if(ip == 0) time_precon = time_precon - omp_get_wtime();
	
	ic= 0;
	ip1= ip*NCOLORtot + ic;
#pragma omp simd
	for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	  TGT_PRECON[Z][i]= TGT_PRECON[Z][i] * Ws_DD[i];
#pragma omp barrier

	for(ic=1; ic<NCOLORtot-1; ic++){
	  ip1= ip*NCOLORtot + ic;
	  for(k=0;k<3;k++)
#pragma omp simd
	    for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	      TGT_PRECON[Z][i] = TGT_PRECON[Z][i] - AL_pre[k][i] * TGT_PRECON[Z][itemL[k][i]];
#pragma omp simd
	  for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
            TGT_PRECON[Z][i] = TGT_PRECON[Z][i] * Ws_DD[i];
#pragma omp barrier
	}

	ic = NCOLORtot-1;
	ip1= ip*NCOLORtot + ic;
	for(k=0;k<6;k++)
#pragma omp simd
	  for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	    TGT_PRECON[Z][i] = TGT_PRECON[Z][i] - AL_pre[k][i] * TGT_PRECON[Z][itemL[k][i]];
#pragma omp simd
	for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	  TGT_PRECON[Z][i] = TGT_PRECON[Z][i] * Ws_DD[i];
#pragma omp barrier
	
/* #pragma omp master */
/* 	{ */
/* 	  if ((fp = fopen("forward_C.txt", "w")) == NULL) { */
/* 	    printf("file open error!!\n"); */
/* 	    exit(EXIT_FAILURE); */
/* 	  } */
/* 	  for(i=0;i<N;i++) */
/* 	    fprintf(fp, "%7d%12.6lf\n", i+1, TGT_PRECON[Z][i]); */
/* 	  fclose(fp); */
/* 	} */
/* #pragma omp barrier */


	for(ic=NCOLORtot-1; ic>0; ic--){
	  ip1= ip*NCOLORtot + ic;
	  for(k=0;k<3;k++)
#pragma omp simd
	    for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	      ZD[i] = ZD[i] + AU_pre[k][i] * TGT_PRECON[Z][itemU[k][i]];
#pragma omp simd
	  for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
            TGT_PRECON[Z][i]= TGT_PRECON[Z][i] - Ws_DD[i]*ZD[i];
#pragma omp barrier
	}

	ic = 0;
	ip1= ip*NCOLORtot + ic;
	for(k=0;k<6;k++)
#pragma omp simd
	  for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	    ZD[i] = ZD[i] + AU_pre[k][i] * TGT_PRECON[Z][itemU[k][i]];
#pragma omp simd
	for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	  TGT_PRECON[Z][i]= TGT_PRECON[Z][i] - Ws_DD[i]*ZD[i];
#pragma omp barrier

	if(ip == 0) time_precon = time_precon + omp_get_wtime();

/* #pragma omp master */
/* 	{ */
/* 	  if ((fp = fopen("backward_C.txt", "w")) == NULL) { */
/* 	    printf("file open error!!\n"); */
/* 	    exit(EXIT_FAILURE); */
/* 	  } */
/* 	  for(i=0;i<N;i++) */
/* 	    fprintf(fp, "%7d%12.6lf\n", i+1, TGT_PRECON[Z][i]); */
/* 	  fclose(fp); */
/* 	} */
/* #pragma omp barrier */

//===

//
// +-------------+
// | RHO= {r}{z} |
// +-------------+
//===
	W_RHO[ip]= 0.0;
#pragma omp simd            
	for(i = ip*ls; i < rmin; i++)
	  W_RHO[ip]= W_RHO[ip] + W[R][i]*TGT_PRECON[Z][i];
	RHO = 0.0;
#pragma omp barrier
#pragma omp simd
	for(i=0; i < nth; i++)
	  RHO= RHO + W_RHO[i];
	// if(ip == 0) printf("Check rho = %lf\n",RHO);
//C===;

//C;
//C +-----------------------------+;
//C | {p} = {z} if      ITER=1    |;
//C | BETA= RHO / RHO1  otherwise |;
//C +-----------------------------+;
//C===;
	if(L == 0){
#pragma omp simd
	  for(i = ip*ls; i < rmin; i++)
	    W[P][i] = TGT_PRECON[Z][i];
	}else{
	  BETA = RHO / RHO1;
#pragma omp simd
	  for(i = ip*ls; i < rmin; i++)
	    W[P][i] = W[Z][i] + BETA * TGT_PRECON[P][i];
      }
#pragma omp barrier
	// if(ip == 0) printf("Check beta = %lf\n",BETA);

//
// +-------------+
// | {q}= [A]{p} |
// +-------------+
//===        
    for(ic=0; ic<NCOLORtot; ic++){
      ip1= ip*NCOLORtot + ic;
#pragma omp simd
	for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	  W[Q][i] = D[i]*W[P][i];

	if (ic == 0){
	  for(k=0;k<6;k++)
#pragma omp simd            
	    for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	      W[Q][i]= W[Q][i] + AU[k][i]*W[P][itemU[k][i]];
	  
	}else if (ic < NCOLORtot-1){
	  for(k=0;k<3;k++)
#pragma omp simd            
	    for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	      W[Q][i]= W[Q][i] + AU[k][i]*W[P][itemU[k][i]]
		               + AL[k][i]*W[P][itemL[k][i]];

	}else{
	  for(k=0;k<6;k++)
#pragma omp simd            
	    for(i=SMPindex[ip1]; i<SMPindex[ip1+1]; i++)
	      W[Q][i]= W[Q][i] + AL[k][i]*W[P][itemL[k][i]];
	}
	// if(L==0) printf("Check SMPindex ip=%d, ic=%d, stat=%d, end=%d\n", ip, ic, SMPindex[ip1], SMPindex[ip1+1]);
    }
    
//C
//C +---------------------+;
//C | ALPHA= RHO / {p}{q} |;
//C +---------------------+;
//C===
#pragma omp barrier
	W_C1[ip]= 0.0;
#pragma omp simd            
	for(i = ip*ls; i < rmin; i++)
	  W_C1[ip]= W_C1[ip] + W[P][i] * W[Q][i];
	C1 = 0.0;
#pragma omp barrier
#pragma omp simd
	for(i=0; i < nth; i++)
	  C1= C1 + W_C1[i];
	ALPHA= RHO / C1;
	/* if(ip == 0){ */
	/*   double temp1=0.0,temp2=0.0; */
	/*   for(i=0;i<N;i++) */
	/*     temp1 = temp1 + W[P][i] * W[P][i]; */
	/*   for(i=0;i<N;i++) */
	/*     temp2 = temp2 + W[Q][i] * W[Q][i]; */
	/*   printf("Check alpha = %lf, %lf, %lf, %lf, %lf\n",ALPHA, C1, RHO, temp1, temp2); */
	/*   printf("Check C1 "); */
	/*   for(i=0;i<nth;i++) */
	/*     printf(", %lf", W_C1[i]); */
	/*   printf("\n"); */
	/* } */
#pragma omp barrier
//C===
//C
//C +----------------------+;
//C | {x}= {x} + ALPHA*{p} |;
//C | {r}= {r} - ALPHA*{q} |;
//C +----------------------+;
//C===;
	W_DNRM2[ip] = 0.0;
#pragma omp simd
	for(i = ip*ls; i < rmin; i++){
	  X[i] = X[i] + ALPHA * W[P][i];
	  W[R][i]= W[R][i] - ALPHA * W[Q][i];
	  W_DNRM2[ip]= W_DNRM2[ip] + W[R][i] * W[R][i];
	}
	DNRM2 = 0.0;
#pragma omp barrier
#pragma omp simd
	for(i=0; i < nth; i++)
	  DNRM2= DNRM2 + W_DNRM2[i];
//C===;

	ERR = sqrt(DNRM2/BNRM2);
	if (L%100 == 0){
#pragma omp single
	  printf("%i, %e\n", L, ERR);
	}
      
	if(ERR < EPS){
	  IER = 0;
	  break;
	}else{
	  RHO1 = RHO;
	}
	
      }

    IER = 1;
    if(ip == 0){
      ITR = L;
      ERR2 = ERR;
    }
  }

  Etime= omp_get_wtime();
  
  printf("Converged, %i, %e\n", ITR, ERR2);
  printf("%e12.4 sec. (solver)\n", Etime-Stime);
  printf("%e12.4 sec. (preconditioner)\n", time_precon);
}
  
