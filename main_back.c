//decrement itemL, itemU
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "precision.h"

#define CHUNK 128

#define type_CRS 1
#define type_ELL 2
#define type_SCS 3

typedef struct{
  int N, N2, NPL, NPU, NCOLORtot, PEsmpTOT, EPS, ITR, IER;
  int *indexL, *itemL, *indexU, *itemU, *SMPindex;
  mat_cg *D, *AL, *AU;
  vec_cg *B, *X;
}st_CRS;

typedef struct{
  int N, N2, NCOLORtot, PEsmpTOT, EPS, ITR, IER;
  int **itemL, **itemU, *SMPindex;
  mat_cg *D, **AL, **AU;
  vec_cg *B, *X;
}st_ELL;

typedef struct{
  int N, N2, NCOLORtot, PEsmpTOT, EPS, ITR, IER, BLKsiz, NSIMD;
  int ***itemL, ***itemU, *SMPindex;
  mat_cg *D, ***AL, ***AU;
  mat_precon ***AL_pre, ***AU_pre;
  vec_cg *B, *X;
}st_SCS;

void solve_ICCG_CRS(int N, int N2, int NPL, int NPU, int *indexL, int *itemL, int *indexU, int *itemU,
		    mat_cg *D, vec_cg *B, vec_cg *X, mat_cg *AL, mat_cg *AU,
		    int NCOLORtot, int PEsmpTOT, int *SMPindex, int EPS, int ITR, int IER);
void solve_ICCG_ELL(int N, int N2, int *itemL[N+N2], int *itemU[N+N2], mat_cg *D, vec_cg *B, vec_cg *X, mat_cg *AL[N+N2], mat_cg *AU[N+N2],
		    int NCOLORtot, int PEsmpTOT, int *SMPindex, int EPS, int ITR, int IER);
void solve_ICCG_SCS(int N, int N2, int BLKsiz, int *itemL[6][CHUNK*BLKsiz], int *itemU[6][CHUNK*BLKsiz],
		    mat_cg *D, vec_cg *B, vec_cg *X, mat_cg *AL[6][CHUNK*BLKsiz], mat_cg *AU[6][CHUNK*BLKsiz],
		    mat_precon *AL_pre[6][CHUNK*BLKsiz], mat_precon *AU_pre[6][CHUNK*BLKsiz],
		    int NCOLORtot, int PEsmpTOT, int *SMPindex, int EPS, int ITR, int IER);
void read_mm(char *fname, int *type, st_CRS *dat_CRS, st_ELL *dat_ELL, st_SCS *dat_SCS);
  
int main(int argc, char *argv[])
{
  int type;
  char *fname;
  st_CRS dat_CRS;
  st_ELL dat_ELL;
  st_SCS dat_SCS;
  
  if(argc != 2){
    printf("Required input file\n");
    exit(1);
  }
  fname = argv[1];
  
  read_mm(fname, &type, &dat_CRS, &dat_ELL, &dat_SCS);

  //decrement
    
  switch(type){
  case type_CRS:
    solver_ICCG_CRS(dat_CRS.N, dat_CRS.N2, dat_CRS.NPL, dat_CRS.NPU, dat_CRS.indexL, dat_CRS.itemL, dat_CRS.indexU,
		    dat_CRS.itemU, dat_CRS.D, dat_CRS.B, dat_CRS.X, dat_CRS.AL, dat_CRS.AU, dat_CRS.NCOLORtot,
		    dat_CRS.PEsmpTOT, dat_CRS.SMPindex, dat_CRS.EPS, dat_CRS.ITR, dat_CRS.IER);
  case type_ELL:
    solver_ICCG_ELL(dat_ELL.N, dat_ELL.N2, dat_ELL.itemL, dat_ELL.itemU, dat_ELL.D, dat_ELL.B, dat_ELL.X,
		    dat_ELL.AL, dat_ELL.AU, dat_ELL.NCOLORtot, dat_ELL.PEsmpTOT, dat_ELL.SMPindex,
		    dat_ELL.EPS, dat_ELL.ITR, dat_ELL.IER);
  case type_SCS:
    solver_ICCG_SCS(dat_SCS.N, dat_SCS.N2, dat_SCS.BLKsiz, dat_SCS.itemL, dat_SCS.itemU, dat_SCS.D, dat_SCS.B,
		    dat_SCS.X, dat_SCS.AL, dat_SCS.AU, dat_SCS.AL_pre, dat_SCS.AU_pre,
		    dat_SCS.NCOLORtot, dat_SCS.PEsmpTOT, dat_SCS.SMPindex, dat_SCS.EPS, dat_SCS.ITR,
		    dat_SCS.IER, dat_SCS.NSIMD);
  default :
    printf("Invalid file format\n");
    exit(1);
  }
  
  return 0;
}

void read_mm(char *fname, int *type, st_CRS *dat_CRS, st_ELL *dat_ELL, st_SCS *dat_SCS){
  int len_type;
  char *name_type;
  FILE *fp;

  if ((fp = fopen(fname, "rb")) == NULL) {
    printf("file open failed\n");
    exit(1);
  }

  fread(&len_type, sizeof(int), 1, fp);
  if((name_type = (char *)malloc((len_type+1) * sizeof(char))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  fread(name_type, sizeof(char), len_type, fp);
  name_type[len_type] = '\0';
  if(strcmp(name_type, "CRS") == 0){
    printf("Storage format is CRS.");
    *type = type_CRS;
    read_CRS(fp, dat_CRS);
  }else if(strcmp(name_type, "ELL") == 0){
    printf("Storage format is ELL.");
    *type = type_ELL;
    read_ELL(fp, dat_ELL);
  }else if(strcmp(name_type, "SCS") == 0){
    printf("Storage format is SCS.");
    *type = type_SCS;	    
    read_SCS(fp, dat_SCS);
  }else{
    printf("Invalid matrix file\n");
    exit(1);
  }

  fclose(fp);  
 
  return 0;
}
void read_CRS(FILE *fp, st_CRS *dat_CRS){

  fread(&dat_CRS->N, sizeof(int), 1, fp);
  fread(&dat_CRS->N2, sizeof(int), 1, fp);
  fread(&dat_CRS->NPL, sizeof(int), 1, fp);
  fread(&dat_CRS->NPU, sizeof(int), 1, fp);

  if((dat_CRS->indexL = (int *)malloc((dat_CRS->N+1) * sizeof(int))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  if((dat_CRS->indexU = (int *)malloc((dat_CRS->N+1) * sizeof(int))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }

  if((dat_CRS->itemL = (int *)malloc(dat_CRS->NPL * sizeof(int))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  if((dat_CRS->itemU = (int *)malloc(dat_CRS->NPU * sizeof(int))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }

  if((dat_CRS->D = (int *)malloc(dat_CRS->N * sizeof(int))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  if((dat_CRS->B = (int *)malloc(dat_CRS->N * sizeof(int))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  if((dat_CRS->X = (int *)malloc(dat_CRS->N * sizeof(int))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }

}
