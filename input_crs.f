!C
!C***
!C*** INPUT
!C***
!C
!C    INPUT CONTROL DATA
!C
      subroutine INPUT_CRS
      use STRUCT_CRS
      use PCG_CRS

!C      implicit REAL*8 (A-H,O-Z)
      implicit none

      integer num_args
      character(10) :: val_read
      character(1000) :: fname
      
      character*80 CNTFIL

!C
!C-- CNTL. file
      open  (11, file='INPUT.DAT', status='unknown')
      read (11,*) NX, NY, NZ
      read (11,*) DX, DY, DZ
      read (11,*) EPSICCG       !, BIG         
      read (11,*) PEsmpTOT
      read (11,*) NCOLORtot
      read (11,*) NFLAG
      close (11)
      PEsmpTOT = omp_get_max_threads()
      write (*,'(//a,i8)')   '### Number of threads    =', PEsmpTOT
!      write (*,'(  a,i8)')   '### N2(Size of padding?) =', N2
!      write (*,'(  a,i8)')   '### Width of SIMD        =', NSIMD
!      write (*,'(  a,1pe16.6)') '### BIG', BIG   
!C===
!     NCOLORk= NCOLORtot
      method = 0

      return
      end
