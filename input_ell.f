!C
!C***
!C*** INPUT
!C***
!C
!C    INPUT CONTROL DATA
!C
      subroutine INPUT_ELL
      use STRUCT_ELL
      use PCG_ELL

      implicit REAL*8 (A-H,O-Z)

      character*80 CNTFIL

!C
!C-- CNTL. file
      open  (11, file='INPUT.DAT', status='unknown')
        read (11,*) NX, NY, NZ
        read (11,*) DX, DY, DZ
        read (11,*) EPSICCG
        read (11,*) PEsmpTOT
        read (11,*) NCOLORtot
        read (11,*) NFLAG
        write (*,'(//a,i8)') '### THREAD number=', PEsmpTOT
      close (11)
      PEsmpTOT = omp_get_max_threads()
!C===
      NCOLORk= NCOLORtot

      return
      end
