#include<stdlib.h>
#include<stdio.h>

#include <omp.h>

#include <math.h>

#include "precision.h"

#ifndef CHUNK
#define CHUNK 256
#endif

#if vec_precon_bit == vec_cg_bit
#define TGT_PRECON W
#else
#define TGT_PRECON WZ
#define flag_prepost
#endif


void solver_ICCG_SCS(int N, int N2, int BLKsiz, int NCOLORtot, int PEsmpTOT,
		     int itemL[PEsmpTOT*NCOLORtot][6][CHUNK*BLKsiz], int itemU[PEsmpTOT*NCOLORtot][6][CHUNK*BLKsiz],
		     mat_cg *D, vec_cg *B, vec_cg *X,
		     mat_cg AL[PEsmpTOT*NCOLORtot][6][CHUNK*BLKsiz], mat_cg AU[PEsmpTOT*NCOLORtot][6][CHUNK*BLKsiz],
		     mat_precon AL_pre[PEsmpTOT*NCOLORtot][6][CHUNK*BLKsiz], mat_precon AU_pre[PEsmpTOT*NCOLORtot][6][CHUNK*BLKsiz],
		     int *SMPindex, double EPS, int ITR, double IER, int NSIMD)
{
  mat_precon *Ws_DD;
  
  int R = 1, Z = 0, Q = 0, P = 2;

  double W_RHO[PEsmpTOT], W_C1[PEsmpTOT], W_DNRM2[PEsmpTOT];
  
  vec_precon VAL_p, SW_p;
  vec_cg VAL;

  int nth, ls, rmin;

  int i, ii, k, L, is, js, ib, ib0, ic, ip, ip0, iq0, ip1, iq1, iq2;
  vec_cg C1, ALPHA, BETA, RHO, RHO1, ERR, ERR2, BNRM2, DNRM2;

  double Stime, Etime, time_precon = 0.0;

  char nvp[10] = name_vec_precon, nmp[10]  = name_mat_precon;

  FILE *fp;
//
// +------+
// | INIT |
// +------+
//===
  if(NSIMD != CHUNK){
    printf("Parameter -DCHUNK between fortran and C is defferent\n");
    printf("Please recompile with same parameter -DCHUNK\n");
    exit(1);
  }
  
  printf("solver_ICCG_ELL, %s-%s\n", nmp, nvp);

  vec_cg (*W)[N+N2];
  if((W = (double(*)[N+N2])malloc((N+N2) * 3 * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  if((Ws_DD = (double *)malloc((N+N2) * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  vec_cg (*WZ)[1];
#ifdef flag_prepost
  if((WZ = (double(*)[1])malloc((N+N2) * sizeof(double))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
#endif

#pragma omp parallel private(ip,i)
  {
    ip = omp_get_thread_num();
    for(i=SMPindex[ip*NCOLORtot]; i < SMPindex[(ip+1)*NCOLORtot]; i++){
      X[i]    = 0.0;
      W[01][i] = 0.0;
      W[1][i] = 0.0;
      W[2][i] = 0.0;
#ifdef flag_prepost
      WZ[Z][i] = 0.0;
#endif
    }
  }

#pragma omp parallel default(none)			\
  shared(D, Ws_DD, AL, itemL, SMPindex)			\
  private(ip,ip0,ip1,iq0,iq1,iq2,i,ib0,VAL,k,ic,ib,is)	\
  firstprivate(NCOLORtot)
  {
    ip = omp_get_thread_num();
    for(ic=0; ic<NCOLORtot; ic++){
      iq1= SMPindex[ip*NCOLORtot + ic];
      iq2= SMPindex[ip*NCOLORtot + ic + 1];
      ip0= ip*NCOLORtot + ic;
      iq0= iq1*CHUNK;
      if (ic == 0){
	for(ib=iq1; ib<iq2; ib++){
	  ib0= (ib-iq1)*CHUNK;
	  for(is=0; is<CHUNK; is++){
	    i= iq0 + ib0 + is;
	    // printf("Check ip=%d, i=%d, D=%lf, ip0=%d, ib0=%d, is=%d\n",ip, i, D[i], iq0, ib0, is);
	    // printf("Check D=%lf\n",D[i]);
	    Ws_DD[i]= 1.0/D[i];
	  }
	}
      }else if(ic < NCOLORtot-1){
	for(ib=iq1; ib<iq2; ib++){
	  ib0= (ib-iq1)*CHUNK;
	  for(is=0; is<CHUNK; is++){
	    i= iq0 + ib0 + is;
	    VAL = D[i];
	    for(k=0;k<3;k++)
	      VAL = VAL - (AL[ip0][k][ib0+is]*AL[ip0][k][ib0+is]) * Ws_DD[itemL[ip0][k][ib0+is]];
	      // printf("Check ip=%d, i=%d, itemL=%d, ip0=%d, k=%d, ib0+is=%d, Ws_DD=%lf, AL=%lf, ib0=%d, is=%d\n",ip, i, itemL[ip0][k][ib0+is], ip0, k, ib0+is, Ws_DD[itemL[ip0][k][ib0+is]], AL[ip0][k][ib0+is], ib0, is);
	      //printf("Check ip=%d, i=%d, ip0=%d, k=%d, ib0+is=%d, b0=%d, is=%d\n",ip, i, ip0, k, ib0+is, ib0, is);
	    Ws_DD[i] = 1.0 / VAL;
	  }
	}
      }else{
	for(ib=iq1; ib<iq2; ib++){
	  ib0= (ib-iq1)*CHUNK;
	  for(is=0; is<CHUNK; is++){
	    i= iq0 + ib0 + is;
	    VAL = D[i];
	    for(k=0;k<6;k++)
	      VAL = VAL - (AL[ip0][k][ib0+is]*AL[ip0][k][ib0+is]) * Ws_DD[itemL[ip0][k][ib0+is]];
	    Ws_DD[i] = 1.0 / VAL;
	  }
	}
      }
#pragma omp barrier
    }
  }
//===

//
// +-----------------------+
// | {r0}= {b} - [A]{xini} |
// +-----------------------+
//===
  BNRM2= 0.0;
#pragma omp parallel for private(i) reduction(+:BNRM2)
  for(i = 0; i < N; i++){
    W[R][i]= B[i];
    BNRM2 = BNRM2 + B[i] * B[i];
  }
//====================
  Stime= omp_get_wtime();
//
//***************************************************************  ITERATION
  ITR = N;

#pragma omp parallel default(none)					\
  shared(B, X, D, AL, AU, AL_pre, AU_pre, W, Ws_DD, itemL, itemU,	\
	 WZ, W_RHO, W_C1, W_DNRM2, SMPindex,				\
	 time_precon, RHO1, IER, ITR, ERR2, fp)				\
  private(ic,ip,ip0,ip1,iq0,iq1,iq2,ib,ib0,is,i,k,rmin,L,		\
	  nth,ls,VAL,VAL_p,SW_p,RHO,DNRM2,C1,ERR,ALPHA,BETA)		\
  firstprivate(N, NCOLORtot, BNRM2, EPS, R, Z, Q, P)
  {
    ip = omp_get_thread_num();
    nth= omp_get_num_threads();
    ls = (N+nth-1)/nth;
    rmin = (ip+1)*ls<N ? (ip+1)*ls : N;
    
    for(L=0; L<ITR; L++)
      {
//
// +----------------+
// | {z}= [Minv]{r} |
// +----------------+
//===      

#pragma omp simd
	for(i = ip*ls; i < rmin; i++){
	  TGT_PRECON[Z][i] = W[R][i];
	}
#pragma omp barrier
	if(ip == 0) time_precon = time_precon - omp_get_wtime();

	ic = 0;
	iq1= SMPindex[ip*NCOLORtot + ic];
	iq2= SMPindex[ip*NCOLORtot + ic+1];
	ip0= ip*NCOLORtot + ic;
	iq0= iq1*CHUNK;
	for(ib=iq1; ib<iq2; ib++){
	  ib0= (ib-iq1)*CHUNK;
	  for(is=0; is<CHUNK; is++){
	    i= iq0 + ib0 + is;
	    TGT_PRECON[Z][i] = TGT_PRECON[Z][i] * Ws_DD[i];
	  }
	}
#pragma omp barrier
	
	for(ic=1; ic<NCOLORtot-1; ic++){
	  iq1= SMPindex[ip*NCOLORtot + ic];
	  iq2= SMPindex[ip*NCOLORtot + ic+1];
	  ip0= ip*NCOLORtot + ic;
	  iq0= iq1*CHUNK;
	  for(ib=iq1; ib<iq2; ib++){
	    ib0= (ib-iq1)*CHUNK;
	    for(is=0; is<CHUNK; is++){
	      i= iq0 + ib0 + is;
	      VAL_p = TGT_PRECON[Z][i];
	      for(k=0;k<3;k++)
		VAL_p = VAL_p - AL_pre[ip0][k][ib0+is] * TGT_PRECON[Z][itemL[ip0][k][ib0+is]];
	      TGT_PRECON[Z][i] = VAL_p * Ws_DD[i];
	    }
	  }
#pragma omp barrier
	}

	ic = NCOLORtot-1;
	iq1= SMPindex[ip*NCOLORtot + ic];
	iq2= SMPindex[ip*NCOLORtot + ic+1];
	ip0= ip*NCOLORtot + ic;
	iq0= iq1*CHUNK;
	for(ib=iq1; ib<iq2; ib++){
	  ib0= (ib-iq1)*CHUNK;
	  for(is=0; is<CHUNK; is++){
	    i= iq0 + ib0 + is;
	    VAL_p = TGT_PRECON[Z][i];
	    for(k=0;k<6;k++)
	      VAL_p = VAL_p - AL_pre[ip0][k][ib0+is] * TGT_PRECON[Z][itemL[ip0][k][ib0+is]];
	    TGT_PRECON[Z][i] = VAL_p * Ws_DD[i];
	  }
	}
#pragma omp barrier

/* #pragma omp master */
/* 	{ */
/* 	  if ((fp = fopen("forward_C.txt", "w")) == NULL) { */
/* 	    printf("file open error!!\n"); */
/* 	    exit(EXIT_FAILURE); */
/* 	  } */
/* 	  for(i=0;i<N;i++) */
/* 	    fprintf(fp, "%7d%12.6lf\n", i+1, TGT_PRECON[Z][i]); */
/* 	  fclose(fp); */
/* 	} */
/* #pragma omp barrier */

	for(ic=NCOLORtot-2; ic>0; ic--){
	  iq1= SMPindex[ip*NCOLORtot + ic];
	  iq2= SMPindex[ip*NCOLORtot + ic+1];
	  ip0= ip*NCOLORtot + ic;
	  iq0= iq1*CHUNK;
	  // if(ic == 8 && ip == 7) printf("Check calc1, ip0=%d, ib0=%d, ib=%d, iq1=%d, iq2=%d\n", ip0, ib0, iq1, iq2);
	  for(ib=iq1; ib<iq2; ib++){
	    ib0= (ib-iq1)*CHUNK;
	    // if(ic == 8 && ip == 7 && ib == 1026) printf("Check calc2, ip0=%d, ib0=%d, ib=%d, iq1=%d, iq2=%d\n", ip0, ib0, iq1, iq2);
	    for(is=0; is<CHUNK; is++){
	      i= iq0 + ib0 + is;
	      SW_p = 0.0;
	      for(k=0;k<3;k++){
		// if(i == 262859) printf("Check val, ic=%d, ip=%d, ip0=%d, k=%k, ib0+is=%d, itemU=%d, SW_p=%lf, AU_pre=%lf, TGT_PRECON=%d\n", ic, ip, ip0, k, ib0+is, itemU[ip0][k][ib0+is], SW_p,AU_pre[ip0][k][ib0+is], TGT_PRECON[Z][itemU[ip0][k][ib0+is]]);
		SW_p = SW_p + AU_pre[ip0][k][ib0+is] * TGT_PRECON[Z][itemU[ip0][k][ib0+is]];
	      }
	      TGT_PRECON[Z][i] = TGT_PRECON[Z][i] - Ws_DD[i] * SW_p;
	    }
	  }
#pragma omp barrier
	}

	ic = 0;
	iq1= SMPindex[ip*NCOLORtot + ic];
	iq2= SMPindex[ip*NCOLORtot + ic+1];
	ip0= ip*NCOLORtot + ic;
	iq0= iq1*CHUNK;
	for(ib=iq1; ib<iq2; ib++){
	  ib0= (ib-iq1)*CHUNK;
	  for(is=0; is<CHUNK; is++){
	    i= iq0 + ib0 + is;
	    SW_p = 0.0;
	    for(k=0;k<6;k++){
	      SW_p = SW_p + AU_pre[ip0][k][ib0+is] * TGT_PRECON[Z][itemU[ip0][k][ib0+is]];
	    }
	    TGT_PRECON[Z][i] = TGT_PRECON[Z][i] - Ws_DD[i] * SW_p;
	  }
	}
#pragma omp barrier
	if(ip == 0) time_precon = time_precon + omp_get_wtime();

/* #pragma omp master */
/* 	{ */
/* 	  if ((fp = fopen("backward_C.txt", "w")) == NULL) { */
/* 	    printf("file open error!!\n"); */
/* 	    exit(EXIT_FAILURE); */
/* 	  } */
/* 	  for(i=0;i<N;i++) */
/* 	    fprintf(fp, "%7d%12.6lf\n", i+1, TGT_PRECON[Z][i]); */
/* 	  fclose(fp); */
/* 	} */
/* #pragma omp barrier */

//===

//
// +-------------+
// | RHO= {r}{z} |
// +-------------+
//===
	W_RHO[ip]= 0.0;
#pragma omp simd            
	for(i = ip*ls; i < rmin; i++)
	  W_RHO[ip]= W_RHO[ip] + W[R][i]*TGT_PRECON[Z][i];
	RHO = 0.0;
#pragma omp barrier
#pragma omp simd
	for(i=0; i < nth; i++)
	  RHO= RHO + W_RHO[i];
	// if(ip == 0) printf("Check rho = %lf\n",RHO);
//C===;

//C;
//C +-----------------------------+;
//C | {p} = {z} if      ITER=1    |;
//C | BETA= RHO / RHO1  otherwise |;
//C +-----------------------------+;
//C===;
	if(L == 0){
#pragma omp simd
	  for(i = ip*ls; i < rmin; i++)
	    W[P][i] = TGT_PRECON[Z][i];
	}else{
	  BETA = RHO / RHO1;
#pragma omp simd
	  for(i = ip*ls; i < rmin; i++)
	    W[P][i] = W[Z][i] + BETA * TGT_PRECON[P][i];
	}
	// if(ip == 0) printf("Check beta = %lf\n",BETA);
#pragma omp barrier

//
// +-------------+
// | {q}= [A]{p} |
// +-------------+
//===        

	for(ic=0; ic<NCOLORtot; ic++){
	  iq1= SMPindex[ip*NCOLORtot + ic];
	  iq2= SMPindex[ip*NCOLORtot + ic+1];
	  ip0= ip*NCOLORtot + ic;
	  iq0= iq1*CHUNK;
	  if(ic == 0){
	    for(ib=iq1; ib<iq2; ib++){
	      ib0= (ib-iq1)*CHUNK;
	      for(is=0; is<CHUNK; is++){
		i= iq0 + ib0 + is;
		W[Q][i]= D[i]*W[P][i];
		for(k=0;k<6;k++)
		  W[Q][i] = W[Q][i] + AU[ip0][k][ib0+is] * W[P][itemU[ip0][k][ib0+is]];
	      }
	    }
	  }else if(1 <= ic && ic <= NCOLORtot-2){
	    for(ib=iq1; ib<iq2; ib++){
	      ib0= (ib-iq1)*CHUNK;
	      for(is=0; is<CHUNK; is++){
		i= iq0 + ib0 + is;
		W[Q][i]= D[i]*W[P][i];
		for(k=0;k<3;k++)
 		  W[Q][i] = W[Q][i] + AU[ip0][k][ib0+is] * W[P][itemU[ip0][k][ib0+is]]
		                    + AL[ip0][k][ib0+is] * W[P][itemL[ip0][k][ib0+is]];
	      }
	    }
	  }else{ //ic == NCOLORtot-1
	    for(ib=iq1; ib<iq2; ib++){
	      ib0= (ib-iq1)*CHUNK;
	      for(is=0; is<CHUNK; is++){
		i= iq0 + ib0 + is;
		W[Q][i]= D[i]*W[P][i];
		for(k=0;k<6;k++)
		  W[Q][i] = W[Q][i] + AL[ip0][k][ib0+is] * W[P][itemL[ip0][k][ib0+is]];
	      }
	    }
	  }
	}
#pragma omp barrier
/* #pragma omp master */
/* 	{ */
/* 	  if ((fp = fopen("matvec_C.txt", "w")) == NULL) { */
/* 	    printf("file open error!!\n"); */
/* 	    exit(EXIT_FAILURE); */
/* 	  } */
/* 	  for(i=0;i<N;i++) */
/* 	    fprintf(fp, "%7d%12.6lf\n", i+1, W[Q][i]); */
/* 	  fclose(fp); */
/* 	} */
/* #pragma omp barrier */
//===

//
// +---------------------+
// | ALPHA= RHO / {p}{q} |
// +---------------------+
//===
	W_C1[ip]= 0.0;
#pragma omp simd            
	for(i = ip*ls; i < rmin; i++)
	  W_C1[ip]= W_C1[ip] + W[P][i] * W[Q][i];
	C1 = 0.0;
#pragma omp barrier
#pragma omp simd
	for(i=0; i < nth; i++)
	  C1= C1 + W_C1[i];
	ALPHA= RHO / C1;
	/* if(ip == 0){ */
	/*   double temp1=0.0,temp2=0.0; */
	/*   for(i=0;i<N;i++) */
	/*     temp1 = temp1 + W[P][i] * W[P][i]; */
	/*   for(i=0;i<N;i++) */
	/*     temp2 = temp2 + W[Q][i] * W[Q][i]; */
	/*   printf("Check alpha = %lf, %lf, %lf, %lf, %lf\n",ALPHA, C1, RHO, temp1, temp2); */
	/*   printf("Check C1 "); */
	/*   for(i=0;i<nth;i++) */
	/*     printf(", %lf", W_C1[i]); */
	/*   printf("\n"); */
	/* } */
//C===
//C
//C +----------------------+;
//C | {x}= {x} + ALPHA*{p} |;
//C | {r}= {r} - ALPHA*{q} |;
//C +----------------------+;
//C===;
	W_DNRM2[ip] = 0.0;
#pragma omp simd
	for(i = ip*ls; i < rmin; i++){
	  X[i] = X[i] + ALPHA * W[P][i];
	  W[R][i]= W[R][i] - ALPHA * W[Q][i];
	  W_DNRM2[ip]= W_DNRM2[ip] + W[R][i] * W[R][i];
	}
	DNRM2 = 0.0;
#pragma omp barrier
#pragma omp simd
	for(i=0; i < nth; i++)
	  DNRM2= DNRM2 + W_DNRM2[i];
//C===;

	ERR = sqrt(DNRM2/BNRM2);
	if (L%100 == 0){
#pragma omp single
	  printf("%i, %e\n", L, ERR);
	}
	
	if(ERR < EPS){
	  IER = 0;
	  break;
	}else{
	  RHO1 = RHO;
	}
	
      }
    
    IER = 1;
    if(ip == 0){
      ITR = L;
      ERR2 = ERR;
    }
  }

  Etime= omp_get_wtime();
  
  printf("Converged, %i, %e\n", ITR, ERR2);
  printf("%e sec. (solver)\n", Etime-Stime);
  printf("%e sec. (preconditioner)\n", time_precon);
}
