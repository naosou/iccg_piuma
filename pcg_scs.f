#include "precision.h"
      
      module PCG_SCS
      use iso_c_binding

      integer :: N2, NSIMD, Ntotal
      integer :: NUmax, NLmax, NCOLORtot, NCOLORk, NU, NL
      integer :: NPL, NPU
      integer :: METHOD, ORDER_METHOD, NFLAG, BLKsiz

      real(kind=8) :: EPSICCG

      vec_cg_f, dimension(:), allocatable,target::D, PHI, BFORCE
      vec_cg_f, dimension(:,:), allocatable :: WW
      mat_cg_f, dimension(:,:,:), allocatable, target :: AL, AU
      mat_precon_f, dimension(:,:,:), allocatable, target :: AL0, AU0

      integer, dimension(:), allocatable :: INL, INU, COLORindex
      integer, dimension(:), allocatable :: SMPindex, SMPindexG
      integer, dimension(:), allocatable :: OLDtoNEW, NEWtoOLD
      integer, dimension(:), allocatable :: OLDtoNEWsimd, NEWtoOLDsimd
      integer, dimension(:), allocatable :: BLK_SIMD_index
      integer, dimension(:), allocatable, target :: SMP_SIMD_index

      integer, dimension(:,:), allocatable :: IAL, IAU

      integer, dimension(:), allocatable :: SMPindex_new
      integer, dimension(:  ), allocatable :: INLnew, INUnew
      integer, dimension(:,:), allocatable :: IALnew, IAUnew

      integer, dimension(:), allocatable :: OLDtoNEWnew, NEWtoOLDnew

      integer, dimension(:),   allocatable :: indexL, indexU
      integer, dimension(:,:,:), allocatable, target ::  itemL,  itemU
      integer, dimension(:),   allocatable :: indexL0, indexU0

      end module PCG_SCS
